
import firebase from "../config/firebase";
import "firebase/compat/firestore";
import Order from "../models/Order";

export default class OrderController {
    static async loadOrders(): Promise<Order[]> {
        try {
            var data = await firebase.firestore().collection('orders').get();
            return data.docs.map((element) => {
                return new Order(
                    element.id,
                    element.get("userId"),
                    element.get("books"),
                    element.get("productMoney"),
                    element.get("totalMoney"),
                    element.get("createDate").toDate(),
                    element.get("status"),
                    element.get("paid"),
                    element.get("province"),
                    element.get("district"),
                    element.get("wards"),
                    element.get("address"),
                    element.get("phone"),
                );
            });
        } catch (error) {
            return [];
        }
    }
    static async loadUserOrders(userId: string): Promise<Order[]> {
        try {
            var data = await firebase.firestore().collection('orders').where("userId", "==", userId).get();
            return data.docs.map((element) => {
                return new Order(
                    element.id,
                    element.get("userId"),
                    element.get("books"),
                    element.get("productMoney"),
                    element.get("totalMoney"),
                    element.get("createDate").toDate(),
                    element.get("status"),
                    element.get("paid"),
                    element.get("province"),
                    element.get("district"),
                    element.get("wards"),
                    element.get("address"),
                    element.get("phone"),
                );
            }).filter(element => element.status !== "Cancel");
        } catch (error) {
            return [];
        }
    }
    static async uploadOrder(order: Order): Promise<boolean> {
        try {
            await firebase.firestore().collection('orders').add({
                "books": order.books,
                "productMoney": order.productMoney,
                "totalMoney": order.totalMoney,
                "createDate": order.createDate,
                "status": order.status,
                "paid": order.paid,
                "province": order.province,
                "district": order.district,
                "wards": order.wards,
                "address": order.address,
                "phone": order.phone,
                "userId": order.userId,
            });
            return true;
        } catch (error) {
            return false;
        }
    }

    static async updateOrderStatus(orderId: string, status: 'Waiting' | 'Proccessing' | 'Arriving' | 'Success' | 'Cancel'): Promise<boolean> {
        try {
            if (status === 'Arriving') {
                await firebase.firestore().collection('orders').doc(orderId).update({
                    "status": status,
                    "paid": true,
                });
            }
            else {
                await firebase.firestore().collection('orders').doc(orderId).update({
                    "status": status,
                });
            }            
            return true;
        } catch (error) {
            return false;
        }
    }
    static async loadOrder(orderId: string): Promise<Order | null> {
        try {
            var element= await firebase.firestore().collection('orders').doc(orderId).get();
            // console.log(element);
                return new Order(
                    element.id,
                    element.get("userId"),
                    element.get("books"),
                    element.get("productMoney"),
                    element.get("totalMoney"),
                    element.get("createDate").toDate(),
                    element.get("status"),
                    element.get("paid"),
                    element.get("province"),
                    element.get("district"),
                    element.get("wards"),
                    element.get("address"),
                    element.get("phone"),
                );
            
        } catch (error) {
            return null;
        }
    }
}