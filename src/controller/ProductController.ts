import Book, { Genre, Publisher } from '../models/Book'
import Comment from '../models/Comment'
import firebase from '../config/firebase'
import 'firebase/compat/firestore'

interface BookMap {
    [state: string]: Book
}

interface UserMap {
    [state: string]: {
        username: string
        avt: string
    }
}

export default class ProductController {
    // filter list of books
    static async loadBooks(publisher?: Publisher, genre?: Genre) : Promise<Book[]> {
        try {
            var colectionRef: firebase.firestore.CollectionReference | firebase.firestore.Query 
                = firebase.firestore().collection('books');
            if (publisher) {
                colectionRef = colectionRef.where('publisher', '==', publisher);
            }
            if (genre) {
                colectionRef = colectionRef.where('genre', '==', genre);
            }
            var data = await colectionRef.get()
            
            return data.docs.map((doc) => {        
                return new Book(
                    doc.id,
                    doc.data().name,
                    doc.data().quantity,
                    doc.data().price,
                    doc.data().publisher,
                    doc.data().genre,
                    doc.data().createDate,
                    doc.data().image,
                    doc.data().description
                )
            })
        } catch (error) {
            return []
        }
    }

    static bookFilter(books: Book[], moneyFrom: number, moneyTo: number): Book[] {
        return books.filter(ele => 
            ele.price >= moneyFrom && ele.price <= moneyTo
        );
    }

    // upload new Book to firebase
    static async uploadBook(book: Book): Promise<boolean> {
        try {
        firebase.firestore().collection('books').doc().set({
            name: book.name,
            quantity: book.quantity,
            price: book.price,
            publisher: book.publisher,
            genre: book.genre,
            createDate: new Date(),
            image: book.image,
            description: book.description,
        })
        return true
        } catch (error) {
        console.log('uploadBook failed')
        return false
        }
    }

    // retrieve Book by bookId from firebase
    static async loadBook(bookId: string): Promise<Book | null> {
        try {
        var bookData = await firebase
            .firestore()
            .collection('books')
            .doc(bookId)
            .get()
        if (bookData.exists) {
            var book = new Book(
            bookData.id,
            bookData.get('name'),
            bookData.get('quantity'),
            bookData.get('price'),
            bookData.get('publisher'),
            bookData.get('genre'),
            bookData.get('createDate').toDate(),
            bookData.get('image'),
            bookData.get('description')
            )
            return book
        } else return null
        } catch (error) {
        console.log('loadBook failed.')
        return null
        }
        //return new Book('', '', 0, 0, 'Nhã Nam', 'Self-help', new Date(), '');
    }

    //retrieve collections Comment in Book by bookId
    static async loadBookComment(bookId: string): Promise<Comment[]> {
        let result: Comment[] = []
        try {
        await firebase
            .firestore()
            .collection('books')
            .doc(bookId)
            .collection('comments')
            .orderBy('date', 'desc')
            .get()
            .then((snap) => {
            snap.forEach((doc) => {
                // console.log(doc.data())
                result.push(
                new Comment(
                    doc.id,
                    doc.data().owner,
                    doc.data().date.toDate(),
                    doc.data().vote,
                    doc.data().content,
                    doc.data().bookId
                )
                )
            })
            })
        } catch (error) {
        console.log('loadBookComment failed.')
        }
        return result
    }

    // upload Comment
    static async uploadComment(comment: Comment): Promise<boolean> {
        try {
        await firebase
            .firestore()
            .collection('books')
            .doc(comment.bookId)
            .collection('comments')
            .add({
            owner: comment.owner,
            date: comment.date,
            vote: comment.vote,
            content: comment.content,
            })
        return true
        } catch (error) {
        console.log('uploadComment failed')
        return false
        }
    }

    static async loadBooksFromIds(ids: string[]): Promise<BookMap> {
        try {
        var result: BookMap = {}
        var data = await firebase
            .firestore()
            .collection('books')
            .where(firebase.firestore.FieldPath.documentId(), 'in', ids)
            .get()
        data.docs.forEach((doc) => {
            result[doc.id] = new Book(
            doc.id,
            doc.data().name,
            doc.data().quantity,
            doc.data().price,
            doc.data().publisher,
            doc.data().genre,
            doc.data().createDate.toDate(),
            doc.data().image,
            doc.data().description
            )
        })
        return result
        } catch (error) {
        return {}
        }
    }

    static async loadUsersFromIds(ids: string[]): Promise<UserMap> {
        try {
        var result: UserMap = {}
        var data = await firebase
            .firestore()
            .collection('users')
            .where(firebase.firestore.FieldPath.documentId(), 'in', ids)
            .get()
        data.docs.forEach((doc) => {
            result[doc.id] = {
            username: doc.get('username'),
            avt: doc.get('avt'),
            }
        })
        return result
        } catch (error) {
        return {}
        }
    }

    static async uploadImage(img: string, bookId: string): Promise<Boolean> {
        try {
            await firebase
            .firestore()
            .collection('books').doc(bookId).update({
                image: img,
            });
            return true;
        }
        catch {
            return false;
        }
    }

    static async convertImageAsync(image: File): Promise<string> {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(image);
            reader.onloadend = () => resolve(reader.result as string);
            reader.onerror = (error) => reject(error);
        });
    }
    static async updateBookById(bookId: string, book: Book): Promise<Boolean> {
      try {
        await firebase
          .firestore()
          .collection('books')
          .doc(bookId)
          .update({
            name: book.name,
            quantity: book.quantity,
            price: book.price,
            publisher: book.publisher,
            genre: book.genre,
            image: book.image,
            description: book.description,
          })
        return true
      } catch (error) {
        return false
      }
    }
    static async deleteBook(bookId: string): Promise<Boolean> {
        try {
          await firebase
            .firestore()
            .collection('books')
            .doc(bookId)
            .update({
              quantity: 0
            })
          return true
        } catch (error) {
          return false
        }
      }  
}
