import User from "../models/User";
import firebase from "../config/firebase";
import "firebase/compat/auth";
import "firebase/compat/firestore";

export default class AuthenticateController {
    private static user: User | null;
    private static isInit = false;

    static getUser(): User | null {
        return this.user;
    }

    private static async loadUser(user: firebase.User): Promise<User | null> {
        try {
            var userData = await firebase.firestore().collection("users").doc(user.uid).get();
            if (!userData.exists) {
                return null;
            }
            else {                
                return new User(
                    userData.id,
                    userData.get('username')  || '',
                    userData.get('email') || user.email || '',
                    userData.get('isManager') || false,
                    userData.get('avt') || user.photoURL || '',
                    userData.get('province') || '',
                    userData.get('district') || '',
                    userData.get('wards') || '',
                    userData.get('address') || '',
                    userData.get('phone') || '',
                    user.emailVerified,
                );
            }
        } catch (error) {
            return null;
        }
    }

    static checkAuthState(onReceive: (user: User | null) => {} ): void {
        if (!this.isInit) {
            firebase.auth().onAuthStateChanged((curUser) => {
                // console.log(curUser)
                if (curUser) {
                    this.loadUser(curUser).then((user) => {
                        this.isInit = true;
                        this.user = user;
                        if (onReceive !== undefined) onReceive(user);
                    }).catch(() => {
                        if (onReceive !== undefined) onReceive(null);
                        this.user = null;
                    });
                }
                else {
                    this.isInit = true;
                    if (onReceive !== undefined) onReceive(null);
                    this.user = null;
                }
            });
        }
    }

    static async login(email: string, password: string): Promise<User | null> {
        try {
            var result = await firebase.auth().signInWithEmailAndPassword(email, password);
            var curUser = result.user
            if (curUser === null) {
                return null
            }
            this.user = await this.loadUser(curUser);
            return this.user;
        } catch (error) {
            return null;
        }
    }

    static async register(email: string, password: string, username: string, phone: string): Promise<User | null> {
        try {
            var result = await firebase.auth().createUserWithEmailAndPassword(email, password);
            if (result.user === null) {
                return null;
            }
            try {
                firebase.firestore().collection('users').doc(result.user.uid).set({
                    'username': username,
                    'email': email,
                    'isManager': false,
                    'avt': '',
                    'province': '',
                    'district': '',
                    'wards': '',
                    'address': '',
                    'phone': phone,
                })
            } catch (error) {
                await result.user.delete()
                return null;
            }
            await result.user.sendEmailVerification()
            return new User(
                result.user.uid,
                username,
                email,
                false,
                '',
                '',
                '',
                '',
                '',
                phone,
                false,
            );
        } catch (error) {
            return null;
        }
    }

    static async changePassword(oldPass: string, newPass: string): Promise<boolean> {
        try {
            if (this.user !== null) {
                var cred = firebase.auth.EmailAuthProvider.credential(this.user.email, oldPass);
                var result = await firebase.auth().currentUser?.reauthenticateWithCredential(cred);
                if (result === undefined) return false;
                var confirmedUser = firebase.auth().currentUser
                if (confirmedUser === null) return false 
                await confirmedUser.updatePassword(newPass);
                return true;
            }
            return false;
        } catch (error) {
            return false;
        }
    }

    static async changeProfileUser(user: User | null){
        try {
            await firebase.firestore().collection("users").doc(user?.userId).update(
                {
                    username: user?.username,
                    phone: user?.phone,
                    address: user?.address,
                    district: user?.district,
                    province: user?.province
                }
            )
            const newUser = await firebase.firestore().collection("users").doc(user?.userId).get();
            if(newUser.exists){
                var tempUser = new User(
                    newUser.id,
                    newUser.get("username"),
                    newUser.get("email"),
                    newUser.get("isManager"),
                    newUser.get("avt"),
                    newUser.get("province"),
                    newUser.get("district"),
                    "",
                    newUser.get("address"),
                    newUser.get("phone"),
                    newUser.get("isVerifie"),
                  )
                  return tempUser;
            }
        } catch (error) {
            console.log(error);
        }
    }

    static async signOut(): Promise<boolean> {
        try {
            await firebase.auth().signOut();
            return true;
        } catch (error) {
            return false;
        }
    }
}
