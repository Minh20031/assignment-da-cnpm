import { ADD_TO_CART, REMOVE_FROM_CART, DECREASE_IN_ORDER, INCREASE_IN_ORDER, RESET_CART } from '../constants'

const initialState = [];

const cart = (state, action) => {
  switch (action.type) {
    case ADD_TO_CART: {
      const { book, amount } = action.payload
      const check = state.some(item => item.book.bookId === book.bookId)

      if (check) {
        const newCart = state.map(item => { return item.book.bookId === book.bookId ? { ...item, amount: item.amount + amount } : item })
        return newCart;
      }
      else {
        return [...state, { book, amount }];
      }
    }
    case DECREASE_IN_ORDER: {
      const { id } = action.payload
      const newCart = state.map(item => { return item.book.bookId === id ? { ...item, amount: item.amount === 1 ? 1 : item.amount - 1 } : item })
      return newCart
    }
    case INCREASE_IN_ORDER: {
      const { id } = action.payload
      const newCart = state.map(item => { return item.book.bookId === id ? { ...item, amount: item.amount + 1 } : item })
      return newCart
    }
    case REMOVE_FROM_CART: {
      const { id } = action.payload
      const newCart = state.filter(item => item.book.bookId !== id)
      return newCart;
    }
    case RESET_CART: {
      return initialState
    }
    default:
      return initialState;
  }
}

export default cart;