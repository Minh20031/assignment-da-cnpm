import React from "react";

import { Grid,Box,CardContent,Card,Typography } from "@mui/material";
import { colors } from "../constants";

// from UserOrder.jsx
const AdminOrderDelivery = (props) => {
  return (
    <Card sx={{ maxWidth: 300 }}>
      <CardContent>
        <Typography
          sx={{
            fontSize: "14px",
            fontWeight: "bold",
            color: colors[8],
          }}
        >
          Giao hàng
        </Typography>

        <br/>

        <Box sx={{ maxWidth: "100%" }}>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography
                sx={{
                  fontSize: "12px",
                  color: colors[8],
                }}
              >
                Mã đơn hàng
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography
                sx={{
                  fontWeight: "medium",
                  fontSize: "12px",
                  color: colors[8],
                }}align="right"
              >
                {props.id}
              </Typography>
            </Grid>

            <Grid item xs={6}>
              <Typography
                sx={{
                  fontSize: "12px",
                  color: colors[8],
                }}
              >
                Địa chỉ
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography
                sx={{
                  fontWeight: "medium",
                  fontSize: "12px",
                  color: colors[8],
                }}align="right"
              >
                {props.address}
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography
                sx={{
                  fontSize: "12px",
                  color: colors[8],
                }}
              >
                Số điện thoại
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography
                sx={{
                  fontWeight: "medium",
                  fontSize: "12px",
                  color: colors[8],
                }}align="right"
              >
                {props.phoneNumber}
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography
                sx={{
                  fontSize: "12px",
                  color: colors[8],
                }}
              >
                Trạng thái
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography
                sx={{
                  fontWeight: "medium",
                  fontSize: "12px",
                  color: colors[1],
                }}align="right"
              >
                {props.state} 
              </Typography>
            </Grid>
          </Grid>
        </Box>
      </CardContent>
    </Card>
  );
};
export default AdminOrderDelivery;
