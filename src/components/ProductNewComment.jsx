import React from 'react'
import {
  Grid,
  Rating,
  Paper,
  Avatar,
  TextField,
  Typography,
} from '@mui/material'
import ButtonComponent from './ButtonComponent'

const ProductNewComment = (props) => {
  return (
    <>
      <Typography variant="h6" mb={2}>
        Đánh giá nhận xét
      </Typography>
      <Grid container flexWrap="noWrap" alignItems="center" spacing={2}>
        <Grid item>
          <Avatar
            alt={props.user?.username}
            src={props.user?.avt || props.user?.username.split('')[0]}
          />
        </Grid>
        <Grid
          item
          container
          flexDirection="row"
          alignItems="self-end"
          sx={{
            minWidth: {
              xs: '400px',
              md: '600px',
              lg: '800px',
            },
          }}
        >
          <Grid item width="100%">
            <Paper variant="outlined">
              <TextField
                fullWidth
                multiline
                value={props.newComment.content}
                onChange={(e) =>
                  props.handleUpdateNewComment(e.target.value, 'content')
                }
              />
            </Paper>
          </Grid>
        </Grid>
      </Grid>
      <Grid
        container
        alignItems="center"
        justifyContent="space-between"
        sx={{ ml: '4.5%', mt: 2, width: '95%' }}
      >
        <Grid item>
          <Rating
            value={parseInt(props.newComment.value)}
            defaultValue={5}
            size="small"
            onChange={(e, number) =>
              props.handleUpdateNewComment(number, 'value')
            }
          />
        </Grid>
        <Grid item>
          <ButtonComponent
            isIcon={false}
            variant="contained"
            color="primaryColor"
            children={<Typography variant="body2">Gửi</Typography>}
            onClick={props.handleCreateNewComment}
          />
        </Grid>
      </Grid>
    </>
  )
}

export default ProductNewComment
