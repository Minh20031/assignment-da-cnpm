import React from 'react'

import { Button, IconButton } from '@mui/material'

const ButtonComponent = (props) => {
  if (props.isIcon) {
    return (
      <IconButton
        size={props.size}
        color={props.color}
        sx={props.isBorder && {
          border: '1px solid',
          borderRadius: '3px',
        }}
        disabled={props.disabled}
        onClick={props.onClick}
      >
        {props.children}
      </IconButton>
    )
  }
  return (
    <div>
      <Button
        sx={props.sx}
        variant={props.variant}
        color={props.color}
        startIcon={props.startIcon}
        endIcon={props.endIcon}
        disabled={props.disabled}
        size={props.size}
        fullWidth={props.fullWidth}
        onClick={props.onClick}
      >
        {props.children}
      </Button>
    </div>
  )
}

export default ButtonComponent

