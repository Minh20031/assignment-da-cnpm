import React from 'react'
import { Typography } from '@mui/material'

const ProductDescription = (props) => {
  const { description } = props
  return (
    <div>
      <Typography paragraph={true} gutterBottom={true}>
        {description}
      </Typography>
    </div>
  )
}

export default ProductDescription
