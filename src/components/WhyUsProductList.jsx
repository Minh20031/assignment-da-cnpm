import React from 'react'
import imgWhyUs1 from '../assets/image/why_us_1.png'
import imgWhyUs2 from '../assets/image/why_us_2.png'
import imgWhyUs3 from '../assets/image/why_us_3.png'
import { styled } from '@mui/system'
import { Box, Grid } from '@mui/material'

const Img = styled('img')({
	margin: 'auto',
	maxWidth: '100%',
	maxHeight: '100%',
})

function WhyUsProductList() {
	return (
		<Grid 
			container spacing={4}
			justifyContent='space-around'
		>
			<Grid 
				item md={4} xs={12}
				sx={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}
			>
				<Img alt="Why Us Free Shipping" src={imgWhyUs1}/>
				<Box sx={{fontSize:18, fontWeight: 600, mt:2}}>FREE SHIPPING</Box>
			</Grid>
			<Grid 
				item xs={12} md={4}
				sx={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}
			>
				<Img alt="Why Us 100% Refund" src={imgWhyUs2}/>
				<Box sx={{fontSize:18, fontWeight: 600, mt: 2}}>100% REFUND</Box>
			</Grid>
			<Grid 
				item md={4} xs={12}
				sx={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}
			>
				<Img alt="Why Us Support 24/7" src={imgWhyUs3}/>
				<Box sx={{fontSize:18, fontWeight: 600, mt: 2}}>SUPPORT 24/7</Box>
			</Grid>
		</Grid>
	)
}

export default WhyUsProductList
