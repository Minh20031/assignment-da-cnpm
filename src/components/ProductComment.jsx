import React from 'react'
import {
  Card,
  CardHeader,
  CardContent,
  Typography,
  Avatar,
  Rating,
  Box,
} from '@mui/material'

const ProductComment = (props) => {
  const { comments } = props

  return (
    <Box sx={{ maxHeight: '400px', overflowY: 'auto' }}>
      {comments.map((c) => {
        return (
          <Card key={c?.commentId} sx={{ mb: '30px' }}>
            <CardHeader
              avatar={
                <Avatar
                  alt={c?.username}
                  src={c?.avt || c.username.split('')[0].toUpperCase()}
                />
              }
              title={
                <Box
                  sx={{
                    display: 'flex',
                    flexDirection: 'column',
                  }}
                >
                  <span>
                    <strong>{c?.username}</strong>
                  </span>
                  <Rating
                    name="read-only"
                    value={c?.vote}
                    readOnly
                    size="small"
                  />
                </Box>
              }
            />
            <CardContent>
              <Typography variant="body2" color="text.secondary" mt={-3}>
                {c?.date.toLocaleDateString()}
              </Typography>
              <Typography variant="body2" color="text.secondary" mt={3}>
                {c?.content}
              </Typography>
            </CardContent>
          </Card>
        )
      })}
    </Box>
  )
}

export default ProductComment
