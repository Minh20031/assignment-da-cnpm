import React from 'react';
import { Box } from '@mui/material';
import { colors } from '../constants';
import CheckCircleRoundedIcon from '@mui/icons-material/CheckCircleRounded';


const ModalContent3 = () => {
    return (
        <Box sx={{ 
            width: '100%',
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            color: colors[1],
            fontSize: "18px",
            fontWeight: "400",
            marginTop: "40px"
        }}>
            <CheckCircleRoundedIcon sx={{
                width: "120px",
                height: "120px",
                color: colors[1],
                marginBottom: "10px"
            }}></CheckCircleRoundedIcon>
            Thành công
        </Box>
    );
}

export default ModalContent3

