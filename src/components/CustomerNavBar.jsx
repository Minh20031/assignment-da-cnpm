import React from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import { colors } from '../constants/index'
import logo from '../assets/image/logo.png'
import { Link } from 'react-router-dom'
import { Box, Typography, Grid, Toolbar, AppBar } from '@mui/material'

const CustomerNavBar = (props) => {
  const history = useHistory()
  const location = useLocation()

  const handleLinkToHomePage = () => {
    history.push('/')
  }
  return (
    <AppBar
      position="static"
      sx={{ boxShadow: 0, backgroundColor: 'whiteColor.main' }}
    >
      <Toolbar>
        <Grid container spacing={2} alignItems="center">
          <Grid item xs={4}>
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'row',
                marginLeft: '10px',
                alignItems: 'center',
                minWidth: '160px',
                marginRight: '20px',
                cursor: 'pointer',
              }}
              onClick={handleLinkToHomePage}
            >
              <img src={logo} alt="Logo" width={45} height={45} />
              <Typography variant="h6">
                <Box
                  sx={{
                    fontWeight: 'bold',
                    paddingLeft: '10px',
                    color: colors[6],
                  }}
                >
                  Book Soul
                </Box>
              </Typography>
            </Box>
          </Grid>
          <Grid item xs={3}>
            <Link to="/" style={{ textDecoration: 'none' }}>
              <Typography
                sx={{
                  fontSize: '18px',
                  color: location.pathname === '/' ? colors[0] : colors[6],
                }}
              >
                TRANG CHỦ
              </Typography>
            </Link>
          </Grid>
          <Grid item xs={3}>
            <Link to="/product" style={{ textDecoration: 'none' }}>
              <Typography
                sx={{
                  fontSize: '18px',
                  color: (location.pathname.split('/')[1] === 'product') ? colors[0] : colors[6],
                }}
              >
                SẢN PHẨM
              </Typography>
            </Link>
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  )
}

export default CustomerNavBar
