import React from 'react';
import { colors } from '../constants/index';
import { Typography, Box, Grid } from '@mui/material';
import logo from '../assets/image/logo.png';
import { FacebookOutlined, Twitter } from '@mui/icons-material';


const Footer = () => {
    return (
        <Box sx={{
            background: colors[2],
            minHeight: '150px',
            padding: '20px',
            color: colors[6],
        }}>
            <Box sx={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
            }}>
                <Box sx={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: '60px',
                    minWidth: '160px',
                    marginRight: '20px'
                }}>
                    <img src={logo} alt="Logo" width={45} height={45}/>
                    <Typography variant="h6">
                        <Box sx={{ 
                            fontWeight: 'bold',
                            paddingLeft: '10px',
                            color: colors[6],
                        }}>Book Soul</Box>
                    </Typography>
                </Box>

                <Box sx={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'end',
                    minWidth: '140px',
                    marginRight: '10px',
                }}>
                    <Grid container spacing={1}>
                        <Grid item xs={12} sm={12} md={6}>
                            <Typography variant="h6" sx={{ 
                                fontWeight: 'bold',
                                fontSize: '18px',
                                color: colors[6],
                            }}>Theo dõi</Typography>
                            <Box>
                                <FacebookOutlined sx={{
                                    color: "#385C8E",
                                    marginRight: '10px',
                                }}></FacebookOutlined>
                                <Twitter sx={{
                                    color: "#03A9F4",
                                }}></Twitter>
                            </Box>
                        </Grid>
                        <Grid item xs={12} sm={12} md={6}>
                            <Typography sx={{ 
                                fontWeight: 'bold',
                                fontSize: '18px',
                                color: colors[6],
                            }}>Liên lạc</Typography>
                            <Typography sx={{
                                fontWeight: 'normal',
                                fontSize: '16px',
                                color: colors[6],
                            }}>
                                01 Võ Văn Ngân, Linh Trung, Thủ Đức, Hồ Chí Minh
                            </Typography>
                        </Grid>
                    </Grid>
                </Box>
            </Box>
        </Box>
    )
}

export default Footer
