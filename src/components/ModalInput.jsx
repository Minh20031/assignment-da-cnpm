import React from 'react';
import { TextField, MenuItem } from '@mui/material';


const ModalInput = (props) => {
    const handleChange = (event) => {
        props.onValueChange(event.target.value);
    };
    if (props.select) {
        return (
            <TextField id="filled-basic" label={props.label} variant="filled" color="secondaryColor" InputProps={{ 
                disableUnderline: true,
            }}
            fullWidth={true}
            value={props.value}
            select
            onChange={handleChange}
            >
                {props.options.map((option, index) => (
                    <MenuItem key={index} value={option}>
                        {option}
                    </MenuItem>
                ))}
            </TextField>
        );
        
    } else {
        return (
            <TextField id="filled-basic" label={props.label} variant="filled" color="secondaryColor" InputProps={{ 
                disableUnderline: true,
            }}
            fullWidth={true}
            value={props.value}
            onChange={handleChange}/>
        );
    }
}

export default ModalInput

