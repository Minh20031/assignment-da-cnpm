
import { Grid, Typography } from "@mui/material";

import TextFieldAuthComponent from "./TextFieldAuthComponent";
import ButtonComponent from "./ButtonComponent";


const FormPasswordComponent = (props) => {

  return (
    <>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <Typography variant="subtitle2">Mật khẩu cũ</Typography>
            </Grid>
            <Grid item xs={12}>
              <TextFieldAuthComponent
                placeholder="Mật khẩu cũ"
                type="password"
                name="oldPassword"
                // value={oldPass}
                onChange={props.handleChangeForm}
              />
            </Grid>
          </Grid>
        </Grid>

        <Grid item xs={12}>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <Typography variant="subtitle2">Mật khẩu mới</Typography>
            </Grid>
            <Grid item xs={12}>
              <TextFieldAuthComponent
                placeholder="Mật khẩu mới"
                type="password"
                name="newPassword"
                onChange={props.handleChangeForm}
              />
            </Grid>
          </Grid>
        </Grid>

        <Grid item xs={12}>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <Typography variant="subtitle2">Xác nhận mật khẩu mới</Typography>
            </Grid>
            <Grid item xs={12}>
              <TextFieldAuthComponent
                placeholder="Xác nhận"
                type="password"
                onChange={props.handleChangeForm}
                name="confirmPassword"
              />
            </Grid>
          </Grid>
        </Grid>

        <Grid item xs={12}>
          <Grid container justifyContent="center">
            <ButtonComponent
              sx={{ px: "100px", py: "20px", my: 2 }}
              variant="contained"
              color="primaryColor"
              children={<Typography variant="body1">Xác nhận</Typography>}
              onClick={props.handleSubmitForm}
            />
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export default FormPasswordComponent;
