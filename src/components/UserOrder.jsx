import React from 'react'
import Card from '@mui/material/Card'
import { CardContent } from '@mui/material'
import Typography from '@mui/material/Typography'
import { Grid } from '@mui/material'
import { colors } from '../constants'

const UserOrder = (props) => {
  return (
    <Card sx={{ maxWidth: 275, borderRadius: '10px', cursor: 'pointer' }}>
      <CardContent>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography
              sx={{
                fontSize: '14px',
                fontWeight: 'bold',
                color: colors[8],
              }}
            >
              {props.id}
            </Typography>
          </Grid>

          <Grid item xs={12}>
            <Typography
              sx={{
                fontSize: '12px',
                color: colors[8],
              }}
            >
              {props.date.toDateString()}
            </Typography>
          </Grid>

          <Grid item xs={12}>
            <hr />
          </Grid>

          <Grid item xs={12}>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <Typography
                  sx={{
                    fontSize: '12px',
                    color: colors[8],
                  }}
                >
                  Trạng thái
                </Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography
                  sx={{
                    fontWeight: 'bold',
                    fontSize: '12px',
                    color: colors[8],
                  }}
                >
                  {props.state}
                </Typography>
              </Grid>

              <Grid item xs={6}>
                <Typography
                  sx={{
                    fontSize: '12px',
                    color: colors[8],
                  }}
                >
                  Sản phẩm
                </Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography
                  sx={{
                    fontWeight: 'bold',
                    fontSize: '12px',
                    color: colors[8],
                  }}
                >
                  {props.number}
                </Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography
                  sx={{
                    fontSize: '12px',
                    color: colors[8],
                  }}
                >
                  Giá
                </Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography
                  sx={{
                    fontWeight: 'bold',
                    fontSize: '12px',
                    color: colors[1],
                  }}
                >
                  {props.cost} $
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  )
}
export default UserOrder
