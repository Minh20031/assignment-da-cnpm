import {useState, useEffect} from 'react'
import SidebarProductListCard from '../components/SidebarProductListCard'
import { Slider, Box} from '@mui/material';
import ButtonComponent from './ButtonComponent';


function SidebarProductList(props) {
  const [rangeSlider, setRangeSlider] = useState([0, 1000]);

  const [publisherItems, setPublisherItems] = useState([])
  const [genreItems, setGenreItems] = useState([])
  // const [publisherIdx, setPublisherIdx] = useState(0);

  const minDistance = 50;

  useEffect(() => {
    var tmp_publishers=[]
    for (const [key, value] of Object.entries(props.publishers)) {
      tmp_publishers.push({name: key, amount: value})
    }
    setPublisherItems(tmp_publishers)
  }, [props.publishers])

  useEffect(() => {
    var tmp_genres=[]
    for (const [key, value] of Object.entries(props.genres)) {
      tmp_genres.push({name: key, amount: value})
    }
    setGenreItems(tmp_genres)
  }, [props.genres])


  const handleChangeSlider = (event, newValue, activeThumb) => {
    if (!Array.isArray(newValue)) {
      return;
    }

    if (activeThumb === 0) {
      setRangeSlider([Math.min(newValue[0], rangeSlider[1] - minDistance), rangeSlider[1]]);
      props.onChangePrice([Math.min(newValue[0], rangeSlider[1] - minDistance), rangeSlider[1]])
    } else {
      setRangeSlider([rangeSlider[0], Math.max(newValue[1], rangeSlider[0] + minDistance)]);
      props.onChangePrice([rangeSlider[0], Math.max(newValue[1], rangeSlider[0] + minDistance)])
    }
  }

  return (
    <Box sx={props.sx}>
      <Box sx={{mb:5, width:270}}>
        <SidebarProductListCard
          handleOnClick={props.onChangePublisher}
          title="NHÀ XUẤT BẢN"
          items={publisherItems}
          idx={0}
        />
      </Box>

      <Box sx={{mb:5}}>
        <SidebarProductListCard
          handleOnClick={props.onChangeGenre}
          title="THỂ LOẠI"
          items={genreItems}
          idx={0}
        />
      </Box>

      <Box fullWidth sx={{backgroundColor: 'grayColor.main',p:2, mb: 2}}>
        <Box sx={{fontWeight:500, fontSize: 20, mb: 2}}>GIÁ</Box>
        <Box sx={{display:'flex', justifyContent: 'space-between'}}>
          <Box>Khoảng giá</Box>
          <Box>
            $ {rangeSlider[0]} - $ {rangeSlider[1]}
          </Box>
        </Box>
        <Box>
          <Slider
            value={rangeSlider}
            onChange={handleChangeSlider}
            valueLabelDisplay="auto"
            disableSwap
            max={1000}
            step={1}
          />
        </Box>
      </Box>
      
      <ButtonComponent 
        fullWidth
        size='large'
        variant='contained'
        onClick={props.handleOnClickFilter}
        sx={{
          color: 'whiteColor.main',
          backgroundColor: 'primaryColor.main'
        }}
      >
        LỌC
      </ButtonComponent>
    </Box>
  )
}

export default SidebarProductList
