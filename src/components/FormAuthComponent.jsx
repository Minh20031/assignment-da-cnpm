import React from 'react'

import { Grid, Typography } from '@mui/material'

import EmailOutlinedIcon from '@mui/icons-material/EmailOutlined'
import LockOutlinedIcon from '@mui/icons-material/LockOutlined'
import PersonIcon from '@mui/icons-material/Person'
import PhoneIcon from '@mui/icons-material/Phone'

import TextFieldAuthComponent from './TextFieldAuthComponent'
import ButtonComponent from './ButtonComponent'

const FormAuthComponent = (props) => {
  return (
    <>
      <Grid container spacing={2} flexDirection="column">
        {!props.isLogin && (
          <>
            <Grid item>
              <TextFieldAuthComponent
                placeholder="Name"
                icon={<PersonIcon />}
                type="text"
                onChange={props.handleChangeForm}
                name="username"
              />
            </Grid>
            <Grid item>
              <TextFieldAuthComponent
                placeholder="Phone"
                icon={<PhoneIcon />}
                type="text"
                onChange={props.handleChangeForm}
                name="phone"
              />
            </Grid>
          </>
        )}
        <Grid item>
          <TextFieldAuthComponent
            placeholder="Email"
            icon={<EmailOutlinedIcon />}
            type="email"
            onChange={props.handleChangeForm}
            name="email"
          />
        </Grid>
        <Grid item>
          <TextFieldAuthComponent
            placeholder="Mật khẩu"
            icon={<LockOutlinedIcon />}
            type="password"
            onChange={props.handleChangeForm}
            name="password"
          />
        </Grid>
        {!props.isLogin && (
          <Grid item>
            <TextFieldAuthComponent
              placeholder="Xác nhận mật khẩu"
              icon={<LockOutlinedIcon />}
              type="password"
              onChange={props.handleChangeForm}
              name="confirmPassword"
            />
          </Grid>
        )}
        <Grid item sx={{ ml: { lg: '20%', xs: '13%' } }}>
          <ButtonComponent
            sx={{ padding: '10px', width: '80%', my: 2 }}
            variant="contained"
            color="primaryColor"
            children={
              <Typography variant="body1">
                {props.isLogin ? 'Đăng nhập' : 'Đăng ký'}
              </Typography>
            }
            onClick={props.handleSubmitForm}
          />
        </Grid>
        <Grid item sx={{ margin: '0 auto' }}>
          <Typography
            variant="caption"
            color="textColor.main"
            sx={{ fontSize: '16px' }}
          >
            {props.isLogin ? 'Chưa có tài khoản?' : 'Đã có tài khoản?'}
          </Typography>
          <Typography
            variant="caption"
            color="primaryColor.main"
            sx={{
              fontSize: '14px',
              cursor: 'pointer',
              display: 'inline-block',
              ml: 1,
              '&:hover': {
                textDecoration: 'underline',
              },
            }}
            onClick={props.handleChangeState}
          >
            {props.isLogin ? 'Đăng ký' : 'Đăng nhập'}
          </Typography>
        </Grid>
      </Grid>
    </>
  )
}

export default FormAuthComponent
