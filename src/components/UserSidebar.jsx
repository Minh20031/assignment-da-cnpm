import React from "react";
import Box from "@mui/material/Box";
import List from "@mui/material/List";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import LocalShippingOutlinedIcon from "@mui/icons-material/LocalShippingOutlined";
import Typography from "@mui/material/Typography";
import { colors } from "../constants";
import { useHistory } from "react-router-dom";

const UserSidebar = (props) => {
  let history = useHistory();

  const [selectedIndex, setSelectedIndex] = React.useState(props.index);

  const handleListItemClick = (event, index) => {
    let path = "/";
    if (index === 0) path = "/user/profile";
    if (index === 1) path = "/user/order";
    if (index === 2) path = "/user/password";
    setSelectedIndex(index);
    history.push(path);
  };
  return (
    <Box sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper", minHeight: "calc(100vh - 184px)", padding: "0px"}}>
      <List component="nav" aria-label="main mailbox folders">
        <ListItemButton
          selected={selectedIndex === 0}
          onClick={(event) => handleListItemClick(event, 0)}
        >
          <ListItemIcon>
            <PersonOutlineIcon sx={{ color: colors[0], fontSize: "28px" }} />
          </ListItemIcon>
          <Typography
            sx={{
              fontWeight: "bold",
              color: colors[8],
            }}
          >
            Thông tin cá nhân
          </Typography>
        </ListItemButton>
        <ListItemButton
          selected={selectedIndex === 1}
          onClick={(event) => handleListItemClick(event, 1)}
        >
          <ListItemIcon>
            <LocalShippingOutlinedIcon
              sx={{ color: colors[0], fontSize: "28px" }}
            />
          </ListItemIcon>
          <Typography
            sx={{
              fontWeight: "bold",
              color: colors[8],
            }}
          >
            Đơn hàng
          </Typography>
        </ListItemButton>
        <ListItemButton
          selected={selectedIndex === 2}
          onClick={(event) => handleListItemClick(event, 2)}
        >
          <ListItemIcon>
            <LockOutlinedIcon sx={{ color: colors[0], fontSize: "28px" }} />
          </ListItemIcon>
          <Typography
            sx={{
              fontWeight: "bold",
              color: colors[8],
            }}
          >
            Đổi mật khẩu
          </Typography>
        </ListItemButton>
      </List>
    </Box>
  );
};

export default UserSidebar;
