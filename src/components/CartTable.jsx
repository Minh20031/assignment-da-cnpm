import React from 'react'
import { TableCell, TableRow, TableHead, TableContainer, TableBody, Table } from '@mui/material';



const CartTable = (props) => {

    return (
        <TableContainer>
            <Table>
                <TableHead>
                    <TableRow>
                    <TableCell align="left"></TableCell>
                    <TableCell align="left">Sản phẩm</TableCell>
                    <TableCell align="center">Giá</TableCell>
                    <TableCell align="center">Số lượng</TableCell>
                    <TableCell align="center">Tổng tiền</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.children}
                </TableBody>
            </Table>
        </TableContainer>
    );
}

export default CartTable
