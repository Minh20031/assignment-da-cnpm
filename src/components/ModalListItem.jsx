import React from 'react';
import { colors } from '../constants';
import { Box, Checkbox, ListItemText, ListItemButton, ListItem, List } from '@mui/material';

export default function ModalListItem(props) {
    const handleToggle = (value) => () => {
        props.onValueChange(value);
    };

    const text = ["Thẻ tín dụng", "Trực tiếp"]
    return (
        <Box sx={{
            color: colors[1],
            fontSize: "16px",
            fontWeight: "light",
        }}>
            Phương thức thanh toán:
            <List dense sx={{ width: '100%', bgcolor: 'background.paper' }}>
                {text.map((value, index) => {
                    return (
                    <ListItem
                        sx={{
                            backgroundColor: (props.checked === index) ? colors[2] : "transparent",
                            color: colors[6]
                        }}
                        key={index}
                        secondaryAction={
                            <Checkbox
                                edge="end"
                                onChange={handleToggle(index)}
                                checked={props.checked === index}
                                sx={{
                                    color: colors[1],
                                    '&.Mui-checked': {
                                        color: colors[1],
                                    },
                                }}
                            />
                        }
                        disablePadding
                    >
                        <ListItemButton>
                            <ListItemText primary={value} />
                        </ListItemButton>
                    </ListItem>
                    );
                })}
            </List>
        </Box>
        
    );
}