import React from 'react'
import { Box, Modal } from '@mui/material';
import ButtonComponent from './ButtonComponent';
import CustomizedSteppers from './CustomizedSteppers'
import ModalHeader from './ModalHeader';

const OrderModal = (props) => {
    return (
        <Modal
            open={props.open}
            onClose={null}
        >
            <Box sx={{
                position: 'absolute',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                maxWidth: 800,
                width: "90%",
                bgcolor: 'background.paper',
                boxShadow: 24,
                height: 560,
                borderRadius: "16px",
                outline: 'none',
                padding: "10px",
            }}>
                <ModalHeader
                    onBack={() => {
                        props.onBack()
                    }}
                    onClose={() => {
                        props.onClose()
                    }}
                />
                <CustomizedSteppers step={props.step} steps={["", "", ""]}/>
                <Box sx={{
                    padding: "40px",
                    height: "340px",
                    overflowY: "auto"
                }}>
                    {props.children[props.step]}
                </Box>
                <Box sx={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "center",
                    marginTop: "20px"
                }}>
                    <ButtonComponent 
                        isIcon={false}
                        color={"secondaryColor"}
                        variant={"contained"}
                        size={"large"}
                        sx={{width: "220px"}}
                        onClick = {
                            props.buttonAction[props.step]
                        }
                    >
                        {props.buttonTitle[props.step]}
                    </ButtonComponent>
                </Box>
            </Box>
        </Modal>
    );
}

export default OrderModal