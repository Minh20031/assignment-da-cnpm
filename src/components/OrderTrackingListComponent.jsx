import React from "react";

import { Grid } from "@mui/material";
import UserOrder from "./UserOrder";


const OrderTrackingListComponent = (props) => {

  return (
    <>
      <Grid container spacing={2} alignContent="center">
        {props.data.map((element, index) => (
          <Grid 
            item xs={6} key={index}
            onClick={() => {
              props.onItemClick(index)
            }}
          >
            <UserOrder
              id={element.orderId}
              date={element.createDate}
              cost={element.totalMoney.toFixed(2)}
              number={element.getProductAmount()}
              state={element.status}
            ></UserOrder>
          </Grid>
        ))}
      </Grid>
    </>
  );
};

export default OrderTrackingListComponent;
