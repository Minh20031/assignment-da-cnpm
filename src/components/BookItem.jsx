import React from 'react'
import { useHistory } from 'react-router-dom'
import imgAvt from '../assets/image/product-item.png'
import { styled } from '@mui/material/styles'
import { Rating, Box, Typography } from '@mui/material'

const Img = styled('img')({
  margin: 'auto',
  display: 'block',
  maxWidth: '100%',
  maxHeight: '100%',
  height: 240,
  width: 240,
  objectFit: 'cover'
})

const BookItem = (props) => {
  // const { bookId, name, price, image, publisher, quantity, vote, comments } = props.product;
  const history = useHistory()
  const { name, price, vote, bookId, image } = props.product
  const handleLinkToProductDetail = () => {
    history.push(`/product/${bookId}`)
  }

  return (
    <Box onClick={handleLinkToProductDetail} sx={{ cursor: 'pointer' }}>
      <Box sx={props.sx}>
        <Box
          sx={{
            display: 'inline-block',
            position: 'relative',
            border: 2,
            borderColor: 'grayColor.main',
          }}
        >
          <Box
            sx={{
              width: 'wrap-content',
              display: 'inline-block',
              px: 1,
              py: 0.5,
              position: 'absolute',
              zIndex: 0,
              backgroundColor: 'dangerColor.main',
              color: 'whiteColor.main',
            }}
          >
            HOT
          </Box>
          <Box
            sx={{
              display: 'flex',
              maxWidth: 240,
              alignItems: 'center',
              flexDirection: 'column',
              zIndex: 'modal',

              fontSize: 18,
              fontWeight: 900,

              px: 1,
              pb: 1,
            }}
          >
            <Img alt="Product image" src={image || imgAvt}sx={{ mt: 0, mb: 3 }}/>
            <Typography
              noWrap
              sx={{
                mb: 0.5,
                color: 'blackColor.main',
                display: 'inline-block',
                maxWidth: '80%',
                fontSize: 18,
                fontWeight: 900,
              }}
            >
              {name}
            </Typography>
            <Rating
              name="read-only"
              value={vote || 2}
              readOnly
              sx={{ mb: 2}}
            />
            <Box sx={{ color: 'secondaryColor.main', mb: 1 }}>${price}</Box>
          </Box>
        </Box>
      </Box>
    </Box>
  )
}

export default BookItem
