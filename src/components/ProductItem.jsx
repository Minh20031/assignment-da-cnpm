import React from "react";
import Card from "@mui/material/Card";
import { CardContent } from "@mui/material";
import Typography from "@mui/material/Typography";
import { Grid } from "@mui/material";
import { colors } from "../constants";

const ProductItem = (props) => {
  const {img, bookName, bookCost} = props.product;
  
  return (
    // <Card sx={{ width: '25vw', height : '7vw'}}> 
    <Card sx={{ maxWidth: 300, maxHeight : 80 }}>
      <CardContent>
          <Grid container spacing={2} alignItems="center" justifyContent="space-between">
            <Grid >
              <img 
                src={img}
                alt="ProductImage"
                style={{
                maxWidth: '70px',
                maxHeight: '60px',
                display: 'block',
                margin: '10px auto',
                marginLeft: '20px'
                }}/>
            </Grid>

            <Grid >
                <Grid justifyContent="flex-end">
                <Typography
                    sx={{
                    fontWeight: "bold",
                    fontSize: "14px",
                    color: colors[8],
                    }}align = "right"
                >                
                    {bookName}
                </Typography>
                </Grid>

                <Grid justifyContent="flex-end">
                <Typography
                    sx={{
                        fontWeight: "bold",
                    fontSize: "14px",
                    color: colors[1],                    
                    }}align = "right"
                >                    
                    ${bookCost}                
                </Typography>
                </Grid>
            </Grid>            
          </Grid>
      </CardContent>
    </Card>
  );
};
export default ProductItem;
