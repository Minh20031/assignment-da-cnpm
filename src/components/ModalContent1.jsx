import React from 'react';
import { Grid } from '@mui/material';
import ModalInput from './ModalInput';
import ModalListItem from './ModalListItem';



const ModalContent1 = (props) => {
    return (
        <Grid container rowSpacing={2} columnSpacing={4}>
            <Grid item xs={12} sm={12} md={6} container spacing={2}>
                <Grid item xs={6} sm={6} md={6}>
                    <ModalInput label={"Tỉnh/Thành"} select={false} value={props.province}
                    onValueChange={(value) => {
                        props.onValueChange(value, props.district, props.wards, props.address, props.phone, props.payType)
                    }}
                    />
                </Grid>
                <Grid item xs={6} sm={6} md={6}>
                    <ModalInput label={"Phường/Xã"} select={false} value={props.wards} 
                    onValueChange={(value) => {
                        props.onValueChange(props.province, props.district, value, props.address, props.phone, props.payType)
                    }}
                    />
                </Grid>
                <Grid item xs={6} sm={6} md={6}>
                    <ModalInput label={"Quận/Huyện"} select={false} value={props.district}
                    onValueChange={(value) => {
                        props.onValueChange(props.province, value, props.wards, props.address, props.phone, props.payType)
                    }}
                    />
                </Grid>
                <Grid item xs={6} sm={6} md={6}>
                    <ModalInput label={"Địa chỉ"} select={false} value={props.address}
                    onValueChange={(value) => {
                        props.onValueChange(props.province, props.district, props.wards, value, props.phone, props.payType)
                    }}
                    />
                </Grid>
            </Grid>
            <Grid item xs={12} sm={12} md={6}>
                <Grid item xs={12} sm={12} md={12}>
                    <ModalInput label={"Số điện thoại"} select={false} value={props.phone}
                    onValueChange={(value) => {
                        props.onValueChange(props.province, props.district, props.wards, props.address, value, props.payType)
                    }}
                    />
                </Grid>
            </Grid>
            <Grid item xs={12} sm={12} md={6} >
                <Grid item xs={12} sm={12} md={12} >
                    <ModalListItem
                        checked = {props.payType}
                        onValueChange={(value) => {
                            props.onValueChange(props.province, props.district, props.wards, props.address, props.phone, value)
                        }}
                    />
                </Grid>
            </Grid>
        </Grid>
    );
}

export default ModalContent1

