import CancelRoundedIcon from '@mui/icons-material/CancelRounded';
import { colors } from '../constants';
import { Box, TableCell, TableRow, Typography } from '@mui/material';
import ButtonComponent from './ButtonComponent';
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';

const CartItem = (props) => {

    return (
        <TableRow
            key={1}
            sx={{
                '> td': {
                    borderBottom: 0,
                    borderTop: 1,
                    borderColor: colors[5]
                }
            }}
        >
            <TableCell align="left">
                <CancelRoundedIcon sx={{
                    color: colors[7],
                    '&:hover': {
                        cursor: 'pointer',
                    }
                }}
                onClick={() => {props.deleteProduct(props.id)}}
                ></CancelRoundedIcon>
            </TableCell>
            <TableCell align="left">
                <Box sx={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'left',
                    alignItems: 'center',
                    height: '60px',
                    minWidth: '160px',
                }}>
                    <Box sx={{
                        marginRight: "10px",
                        height: "60px",
                        width: "55px",
                    }}>
                        <img src={props.image} alt="Logo" style={{
                            width: "100%",
                            height: "100%",
                            borderRadius: "4px",
                            objectFit: "cover"
                        }}/>
                    </Box>
                    {props.name}
                </Box>
            </TableCell>
            <TableCell align="center">{'$' + props.price.toFixed(2)}</TableCell>
            <TableCell align="center">
                <Box
                    mr={12}
                    sx={{
                        display: 'inline-flex',
                        alignItems: 'center',
                        backgroundColor: colors[5],
                        height: '40px',
                        borderRadius: '2px',
                        mr: {
                            xs: 0,
                        },
                    }}
                >
                    <div
                        onClick={() => {props.decrease(props.id)}}
                    >
                        <ButtonComponent
                            isIcon={true}
                            children={<RemoveIcon color="primaryColor" />}
                            size="small"
                            disabled={props.amount === 1}
                        />
                    </div>
                        <Typography variant="body2" sx={{ 
                            padding: '0 20px',
                            width: "8px",
                            display: "flex",
                            justifyContent: "center",
                        }}>
                            {props.amount}
                        </Typography>
                    <div 
                        onClick={() => {props.increase(props.id)}}
                    >
                        <ButtonComponent
                            isIcon={true}
                            children={<AddIcon color="primaryColor" />}
                            size="small"
                        />
                    </div>
                </Box>
            </TableCell>
            <TableCell align="center">{"$" + (props.price * props.amount).toFixed(2)}</TableCell>
        </TableRow>
    );
}

export default CartItem
