import React from "react";
import { colors } from "../constants/index";

import { useHistory,Link,useLocation } from "react-router-dom";
import { Box, Typography, Grid, Toolbar, AppBar } from "@mui/material";

import logo from "../assets/image/logo.png";
//CustomerNavBar
const AdminNavBar = (props) => {
  const history = useHistory()
  const location = useLocation()
  const handleLinkToHomePage = () => {
    history.push('/')
  }
  return (
    <AppBar position="static" sx={{boxShadow: 0, backgroundColor: 'whiteColor.main'}}>
      <Toolbar>
        <Grid container spacing={2} alignItems="center" >
          <Grid item xs={4}>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                marginLeft: "10px",
                alignItems: "center",
                minWidth: "100px",
                marginRight: "20px",
                cursor: 'pointer',
              }}
              onClick={handleLinkToHomePage}
            >
              <img src={logo} alt="Logo" width={45} height={45} />
              <Typography variant="h6">
                <Box
                  sx={{
                    fontWeight: "bold",
                    paddingLeft: "10px",
                    color: colors[6],
                  }}
                >
                  Book Soul
                </Box>
              </Typography>
            </Box>
          </Grid>
          <Grid item xs={4}>
            <Link to="/admin/product" style={{ textDecoration: "none" }}>
              <Typography
                sx={{
                  fontSize: "18px",
                  color: location.pathname.includes("product") ? colors[0] : colors[6],
                }}
              >
                QUẢN LÝ SẢN PHẨM
              </Typography>
            </Link>
          </Grid>
          <Grid item xs={4}>
            <Link to="/admin/order" style={{ textDecoration: "none" }}>
              <Typography
                sx={{
                  fontSize: "18px",
                  color: location.pathname.includes("order") ? colors[0] : colors[6],
                }}
              >
                QUẢN LÝ ĐƠN HÀNG
              </Typography>
            </Link>
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  );
};

export default AdminNavBar;
