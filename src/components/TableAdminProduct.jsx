import React from 'react'

import { Grid, Box, Typography, CardMedia } from "@mui/material";
import { useHistory } from "react-router-dom";
import ButtonComponent from "./ButtonComponent";
import ReceiptOutlinedIcon from '@mui/icons-material/ReceiptOutlined'

const TableHead = () => {
  const history = useHistory();
  return (
    <Grid
      container
      justifyContent="space-between"
      alignItems="center"
      spacing={2}
      sx={{
        pb: 2,
        borderBottom: "1px solid",
        borderBottomColor: "grayColor.main",
      }}
    >
      <Grid item xs={2} align="center">
        <Typography variant="body2" sx={{ fontWeight: 'bold' }}>
          Mã sản phẩm
        </Typography>
      </Grid>
      <Grid item xs={2} align="center">
        <Typography variant="body2" sx={{ fontWeight: 'bold' }}>
          Tên sản phẩm
        </Typography>
      </Grid>
      <Grid item xs={1} align="center">
        <Typography variant="body2" sx={{ fontWeight: 'bold' }}>
          Hình ảnh
        </Typography>
      </Grid>
      <Grid item xs={1} align="center">
        <Typography variant="body2" sx={{ fontWeight: 'bold' }}>
          Số hàng
        </Typography>
      </Grid>
      <Grid item xs={1} align="center">
        <Typography variant="body2" sx={{ fontWeight: 'bold' }}>
          Giá
        </Typography>
      </Grid>
      <Grid item xs={3}>
        <ButtonComponent
          sx={{ px: 2, py: 1, borderRadius: '10px' }}
          startIcon={<ReceiptOutlinedIcon />}
          variant="contained"
          children={
            <Typography variant="body2" sx={{ fontWeight: 'bold' }}>
              Thêm sản phẩm
            </Typography>
          }
          size="small"
          color="primaryColor"
          onClick={() => history.push("/admin/product/:id")}
        />
      </Grid>
    </Grid>
  );
};

const TableAdminProduct = (props) => {
  const history = useHistory();
  const table = props.data.filter(function (c) {
    return c.quantity;
  }).map((c) => {

    return (
      <Grid
        container
        key={c.bookId}
        sx={{
          maxHeight: "200px",
          my: 2,
          borderRadius: "10px",
          backgroundColor: "lightColor.main",
        }}
        justifyContent="space-between"
        alignItems="center"
      >

        <Grid item xs={2} align="center">
          <Typography variant="body2">{c.bookId}</Typography>
        </Grid>

        <Grid item xs={2} align="center">
          <Typography variant="body2">{c.name}</Typography>
        </Grid>

        <Grid item xs={1} align="center" marginTop="10px" marginBottom="10px">
          <CardMedia image={c.image} alt={c.name} component="img" align="center"
          />
          {/* haven't auto size image in CardMedia */}
        </Grid>

        <Grid item xs={1} align="center">
          <Typography variant="body2">{c.quantity}</Typography>
        </Grid>
        <Grid item xs={1} align="center">
          <Typography variant="body2">{c.price}</Typography>
        </Grid>
        <Grid item container xs={3}>
          <Grid item xs={6}>
            <ButtonComponent
              sx={{ borderRadius: "20px", minWidth: "100px" }}
              variant="contained"
              children={<Typography variant="body2">Sửa</Typography>}
              size="small"
              color="primaryColor"
              onClick={() => history.push("/admin/product/" + c.bookId)}
            />
          </Grid>
          <Grid item xs={6}>
            <ButtonComponent
              sx={{ borderRadius: '20px', minWidth: '100px' }}
              variant="contained"
              children={<Typography variant="body2">Xóa</Typography>}
              size="small"
              color="dangerColor"
              onClick={() => props.handleDeleteBook(c.bookId)}
            />
          </Grid>
        </Grid>


      </Grid>
    );

  });
  return (
    <Box>
      <Box>
        <TableHead />
      </Box>
      <Box>{table}</Box>
    </Box>
  );
};

export default TableAdminProduct;
