import React from "react";

import { Grid,Box,CardContent,Card,Typography } from "@mui/material";
import { colors } from "../constants";
// from UserOrder.jsx
const OrderPayment = (props) => {
  return (
    <Card sx={{ maxWidth: 300 }}>
      <CardContent>
        <Typography
          sx={{
            fontSize: "14px",
            fontWeight: "bold",
            color: colors[8],
          }}
        >
          Thanh toán
        </Typography>

        <br/>

        <Box sx={{ maxWidth: "100%" }}>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography
                sx={{
                  fontSize: "12px",
                  color: colors[8],
                }}
              >
                Sản phẩm ({props.numberOfItems})
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography
                sx={{
                  fontWeight: "medium",
                  fontSize: "12px",
                  color: colors[8],
                }}align="right"
              >
                ${props.cost.toFixed(2)}
              </Typography>
            </Grid>

            <Grid item xs={6}>
              <Typography
                sx={{
                  fontSize: "12px",
                  color: colors[8],
                }}
              >
                Phí vận chuyển
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography
                sx={{
                  fontWeight: "medium",
                  fontSize: "12px",
                  color: colors[8],
                }}align="right"
              >
               ${props.shippingFee.toFixed(2)}
              </Typography>
            </Grid>

        </Grid> 

        <br/>
        <Box className="mt-1 mb-1">
          <hr />
        </Box>
        <br/>

        <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography
                sx={{
                    fontWeight: "bold",
                  fontSize: "12px",
                  color: colors[8],
                }}
              >
                Tổng cộng
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography
                sx={{
                  fontWeight: "medium",
                  fontSize: "12px",
                  color: colors[8],
                  
                }}align="right"
              >
                ${(props.cost + props.shippingFee).toFixed(2)}
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography
                sx={{
                fontWeight: "bold",
                  fontSize: "12px",
                  color: colors[8],

                }}
              >
                Tình trạng
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography
                sx={{
                  fontWeight: "medium",
                  fontSize: "12px",
                  color: colors[1],
                }}
                align="right"
              >                  
                {props.paymentState}                 
              </Typography>
             
            </Grid>
          </Grid>
        </Box>
      </CardContent>
    </Card>
  );
};
export default OrderPayment;
