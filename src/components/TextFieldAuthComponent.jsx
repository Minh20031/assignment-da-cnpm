import React from "react";

import { TextField, InputAdornment } from "@mui/material";

const TextFieldAuthComponent = (props) => {
  return (
    <TextField
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">{props.icon}</InputAdornment>
        ),
        endAdornment: (
          <InputAdornment position="end">{props.endIcon}</InputAdornment>
        ),
      }}
      label={props.label}
      placeholder={props.placeholder}
      type={props.type}
      variant="outlined"
      required
      fullWidth
      name={props.name}
      onChange={props.onChange}
      value={props.value}
      onClick={props.onClick}
      disabled={props.disabled}
    />
  );
};

export default TextFieldAuthComponent;
