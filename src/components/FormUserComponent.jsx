import React, { useState } from "react";

import { Grid, Typography } from "@mui/material";

import TextFieldAuthComponent from "./TextFieldAuthComponent";

import AuthenticateController from "../controller/AuthenticateController";
import ButtonComponent from "./ButtonComponent";
import User from "../models/User";

const FormUserComponent = () => {
  let user = AuthenticateController.getUser();

  const initialForm = {
    name: user?.username,
    email: user?.email,
    phone: user?.phone,
    province: user?.province,
    district: user?.district,
    address: user?.address,
  };

  const [form, setForm] = useState(initialForm);
  const handleChangeForm = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };
  const handleSubmit = async () => {
    const newUser = new User(
      user.userId,
      form.name,
      form.email,
      user.isManager,
      user.avt,
      form.province,
      form.district,
      "",
      form.address,
      form.phone,
      user.isVerified
    );
    const success = await AuthenticateController.changeProfileUser(newUser);
    if (success) {
      window.location.reload();
    }
  };

  return (
    <>
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <Typography variant="subtitle2">Tên</Typography>
            </Grid>
            <Grid item xs={12}>
              <TextFieldAuthComponent
                placeholder="Họ và tên"
                type="text"
                name="name"
                value={form.name}
                onChange={handleChangeForm}
              />
            </Grid>
          </Grid>
        </Grid>

        <Grid item xs={6}>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <Typography variant="subtitle2">Email</Typography>
            </Grid>
            <Grid item xs={12}>
              <TextFieldAuthComponent
                disabled={true}
                placeholder="Email"
                type="text"
                value={form.email}
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={6}>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <Typography variant="subtitle2">Số điện thoại</Typography>
            </Grid>
            <Grid item xs={12}>
              <TextFieldAuthComponent
                placeholder="Số điện thoại"
                type="text"
                name="phone"
                value={form.phone}
                onChange={handleChangeForm}
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={6}>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <Typography variant="subtitle2">Tỉnh/thành</Typography>
            </Grid>
            <Grid item xs={12}>
              <TextFieldAuthComponent
                placeholder="Tỉnh/thành"
                type="text"
                name="province"
                value={form.province}
                onChange={handleChangeForm}
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={6}>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <Typography variant="subtitle2">Quận/Huyện</Typography>
            </Grid>
            <Grid item xs={12}>
              <TextFieldAuthComponent
                placeholder="Quận/Huyện"
                type="text"
                name="district"
                value={form.district}
                onChange={handleChangeForm}
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={6}>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <Typography variant="subtitle2">Số nhà, Tên đường</Typography>
            </Grid>
            <Grid item xs={12}>
              <TextFieldAuthComponent
                placeholder="Số nhà, Tên đường"
                type="text"
                name="address"
                value={form.address}
                onChange={handleChangeForm}
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <Grid container justifyContent="center">
            <ButtonComponent
              sx={{ px: "100px", py: "20px", my: 2 }}
              variant="contained"
              color="primaryColor"
              children={<Typography variant="body1">Xác nhận</Typography>}
              onClick={handleSubmit}
            />
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export default FormUserComponent;
