import { Box, MenuItem, Select } from '@mui/material'
import React, {useState} from 'react'
import MenuIcon from '@mui/icons-material/Menu';

function FilterBarProductList(props) {
    const number = props.numberProducts;
    const [sortBy, setSortBy] = useState('Tên');
    return (
        <Box sx={{width: '100%'}}>
            <Box  sx={{display: 'flex', alignItems:'center',
                backgroundColor: 'grayColor.main', py:0.5, px:2}}
            >
                <MenuIcon sx={{display: {xs: 'block', lg: 'none'}, mr:5}}
                    onClick={props.toggleDrawer(true)}
                />
                <Box sx={{mr:5, alignItems: 'center', display: {xs: 'none', sm: 'block'}}}>
                    {number} sản phẩm
                </Box>
                <Box  sx={{display: 'flex', mr:5, alignItems: 'center'}}>
                    <Box sx={{mr:2, display: {xs: 'none', sm: 'block'}}}>Sắp xếp theo</Box>
                    <Select value={sortBy}
                        onChange={(e)=>{
                            props.setSortBy(e.target.value)
                            setSortBy(e.target.value)
                        }}
                        sx={{minWidth:150}}
                    >
                        {['Tên','Giá tiền'].map((option, index) => (
                            <MenuItem key={option} value={option}>
                                {option}
                            </MenuItem>
                        ))}
                    </Select>
                </Box>
                {/* <Box  sx={{display: 'flex', alignItems: 'center'}}>
                    <Box sx={{mr:2}}>Hiển thị</Box>
                    <TextField select>
                        {[1,2,3,4,5].map((option) => (
                            <MenuItem>
                                {option}
                            </MenuItem>
                        ))}
                    </TextField>
                </Box> */}
            </Box>
        </Box>
    )
}

export default FilterBarProductList
