import React, { useState, useEffect,useRef } from 'react'
import { useHistory } from "react-router-dom";
import { Grid, Box, Typography } from '@mui/material'
import ButtonComponent from './ButtonComponent'
import ProductController from '../controller/ProductController';
import Loading from './LoadingComponent';

const TableHead = () => {
  return (
    <Grid
      container
      justifyContent="space-between"
      alignItems="center"
      spacing={2}
      sx={{ pb: 2, borderBottom: '1px solid', borderBottomColor: 'grayColor.main' }}
    >
      <Grid item xs={2} align="center">
        <Typography variant="body2" sx={{ fontWeight: 'bold' }}>
          Mã đơn hàng
        </Typography>
      </Grid>
      <Grid item xs={2} align="center">
        <Typography variant="body2" sx={{ fontWeight: 'bold' }}>
          Người đặt
        </Typography>
      </Grid>
      <Grid item xs={2} align="center">
        <Typography variant="body2" sx={{ fontWeight: 'bold' }}>
          Ngày đặt
        </Typography>
      </Grid>
      <Grid item xs={2} align="center">
        <Typography variant="body2" sx={{ fontWeight: 'bold' }}>
          Tổng tiền
        </Typography>
      </Grid>
      <Grid item xs={2} align="center">
        <Typography variant="body2" sx={{ fontWeight: 'bold' }}>
          Trạng thái
        </Typography>
      </Grid>
      <Grid item xs={2}>

      </Grid>
    </Grid>
  )
}

const TableAdminOrder = (props) => {
  const history = useHistory();
  var tmp = props.data.map((c) => { return c.userId });

  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(true);
  const isUnmounted = useRef(false);
  useEffect(() => {
    return () => {
        isUnmounted.current = true;
    }
}, [])
  useEffect(() => {
    const fetchData = async () => {
      var user = await ProductController.loadUsersFromIds(tmp);
      if (isUnmounted.current) return;
      setUser(user);
      setLoading(false)
    }
    fetchData()
  }, [tmp])
  if (loading) {
    return <Loading />
  }
  const table = props.data.map((c) => {
    var u = user[c.userId]
    if (!u) return null;
    return (
      <Grid
        container
        key={c.orderId}
        sx={{
          height: '80px',
          my: 2,
          borderRadius: '10px',
          backgroundColor: 'lightColor.main',
        }}
        justifyContent="space-between"
        alignItems="center"

      >
        <Grid item xs={2} align="center">
          <Typography variant="body2">{c.orderId}</Typography>
        </Grid>
        <Grid item xs={2} align="center">
          <Typography variant="body2">{u.username}</Typography>
        </Grid>
        <Grid item xs={2} align="center">
          <Typography variant="body2">{ c.createDate.toISOString().replace('-', '/').split('T')[0].replace('-', '/')}</Typography>
        </Grid>
        <Grid item xs={2} align="center">
          <Typography variant="body2">{c.totalMoney.toFixed(2)}</Typography>
        </Grid>
        <Grid item xs={2} align="center">
          <Typography variant="body2">{c.status}</Typography>
        </Grid>
        <Grid item container xs={2}>
          <Grid item xs={12}>
            <ButtonComponent
              sx={{ borderRadius: '20px', minWidth: '100px' }}
              variant="contained"
              children={<Typography variant="body2">Cập nhật</Typography>}
              size="small"
              color="primaryColor"
              onClick={() => history.push("/admin/order/" + c.orderId)}
            />
          </Grid>
        </Grid>
      </Grid>
    )
  })
  return (
    <Box>
      <Box>
        <TableHead />
      </Box>
      <Box>{table}</Box>
    </Box>
  )
}

export default TableAdminOrder
