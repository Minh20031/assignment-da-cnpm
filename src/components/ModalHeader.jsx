import React from 'react';
import { Box } from '@mui/material';
import { colors } from '../constants';
import ButtonComponent from './ButtonComponent';
import ArrowBackRoundedIcon from '@mui/icons-material/ArrowBackRounded';
import CloseRoundedIcon from '@mui/icons-material/CloseRounded';


const ModalHeader = (props) => {
    return (
        <Box>
        <Box sx={{ 
            width: '100%',
            display: "flex",
            justifyContent: "space-between"
        }}>
            <ButtonComponent
                isIcon={true}
                color={"secondaryColor"}
                variant={"contained"}
                size={"large"}
                onClick={props.onBack}
            >
                <ArrowBackRoundedIcon/>
            </ButtonComponent>
            <ButtonComponent
                isIcon={true}
                color={"secondaryColor"}
                variant={"contained"}
                size={"large"}
                onClick={props.onClose}
            >
                <CloseRoundedIcon/>
            </ButtonComponent>
        </Box>
        <Box sx={{ 
            width: '100%',
            display: "flex",
            color: colors[1],
            fontSize: "24px",
            justifyContent: "center",
        }}>
            Tạo đơn hàng
        </Box>
        </Box>
    );
}

export default ModalHeader

