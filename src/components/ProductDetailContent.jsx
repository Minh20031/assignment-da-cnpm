import React from 'react'

import { styled } from '@mui/material/styles'
import { Grid, Typography, Box, Rating, Divider } from '@mui/material'

import RemoveIcon from '@mui/icons-material/Remove'
import AddIcon from '@mui/icons-material/Add'
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined'

import { colors } from '../constants'
import ButtonComponent from './ButtonComponent'
import imgAlt from '../assets/image/product-detail.png'

const Img = styled('img')({
  margin: 'auto',
  display: 'block',
  maxWidth: '100%',
  maxHeight: '100%',
})

const ProductDetailContent = (props) => {
  const { bookId, name, price, image, publisher, quantity, comments } =
    props.product

  const vote = comments.reduce((sum, c) => (sum += c.vote), 0)

  return (
    <Grid container spacing={{ md: 8 }} key={bookId}>
      <Grid item xs={12} md={6} lg={5}>
        <Box sx={{ minWidth: '300px' }}>
          <Img alt={name} src={image || imgAlt} />
        </Box>
      </Grid>
      <Grid
        item
        xs={12}
        lg={7}
        md={6}
        container
        direction="column"
        spacing={4}
        sx={{ mt: { xs: 2 } }}
      >
        <Grid item>
          <Typography variant="h6">{name || 'Amazon'}</Typography>
          <Box sx={{ display: 'flex', alignItems: 'center', mt: 1 }}>
            <Rating
              readOnly
              value={vote / comments.length}
              size="small"
              sx={{
                mr: 3,
              }}
              precision={0.5}
            />
            <Typography variant="body1">{comments.length} đánh giá</Typography>
          </Box>
        </Grid>
        <Divider light variant="middle" sx={{ mt: 3 }} />
        <Grid item>
          <Typography variant="h6" sx={{ color: colors[0] }}>
            <strong>
              {price.toLocaleString('en-IN', {
                style: 'currency',
                currency: 'USD',
              })}
            </strong>
          </Typography>
          <Box>
            <Typography variant="body2" mt={1} mb={1}>
              Tình trạng: {quantity > 0 ? 'Còn hàng' : 'Hết hàng'}
            </Typography>
            <Typography variant="body2">Phân loại: {publisher}</Typography>
          </Box>
        </Grid>
        <Divider light variant="middle" sx={{ mt: 3 }} />
        <Divider light variant="middle" sx={{ mt: 12 }} />
        <Grid
          item
          container
          sx={{
            mt: 3,
          }}
        >
          <Grid item>
            <Box
              mr={12}
              sx={{
                display: 'flex',
                alignItems: 'center',
                backgroundColor: colors[5],
                height: '40px',
                borderRadius: '2px',
                mr: {
                  xs: 0,
                },
              }}
            >
              <div onClick={props.handleMinusAmount}>
                <ButtonComponent
                  isIcon={true}
                  children={<RemoveIcon color="primaryColor" />}
                  size="small"
                  disabled={props.amount === 1}
                />
              </div>
              <Typography variant="body2" sx={{ padding: '0 20px' }}>
                {props.amount}
              </Typography>
              <div onClick={props.handlePlusAmount}>
                <ButtonComponent
                  isIcon={true}
                  children={<AddIcon color="primaryColor" />}
                  size="small"
                />
              </div>
            </Box>
          </Grid>
          <Grid
            item
            sx={{
              ml: {
                xs: 4,
                md: 12,
              },
            }}
          >
            <ButtonComponent
              sx={{ height: '40px', boxShadow: 'none' }}
              variant="contained"
              color="lightColor"
              children={
                <Typography variant="body2" sx={{ color: 'primaryColor.main' }}>
                  Thêm vào đơn
                </Typography>
              }
              startIcon={<ShoppingCartOutlinedIcon color="primaryColor" />}
              size="large"
              onClick={() => props.handleAddToCart(props.product, props.amount)}
            />
          </Grid>
        </Grid>
        <Divider light variant="middle" sx={{ mt: 8 }} />
      </Grid>
    </Grid>
  )
}

export default ProductDetailContent
