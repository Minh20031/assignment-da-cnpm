import React, { useState } from 'react'
import { Box, Tab } from '@mui/material'
import { TabContext, TabList, TabPanel } from '@mui/lab'

import ProductDescription from './ProductDescription'
import ProductComment from './ProductComment'

const ProductPanel = (props) => {
  const { description, comments } = props
  const [value, setValue] = useState('1')

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <Box
        sx={{
          width: '100%',
          typography: 'body1',
          backgroundColor: '#fafafb',
          borderRadius: '5px',
          padding: '10px 20px',
        }}
      >
        <TabContext value={value}>
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <TabList onChange={handleChange} aria-label="lab API tabs example">
              <Tab label={<span>Thông tin chi tiết sản phẩm</span>} value="1" />
              <Tab label={<span>Bình luận ({comments.length})</span>} value="2" />
            </TabList>
          </Box>
          <TabPanel value="1">
            <ProductDescription description={description} />
          </TabPanel>
          <TabPanel value="2">
            <ProductComment comments={comments} />
          </TabPanel>
        </TabContext>
      </Box>
    </div>
  )
}

export default ProductPanel
