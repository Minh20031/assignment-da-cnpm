import React from 'react'
import ClipLoader  from 'react-spinners/ClipLoader'

const Loading = () => {
  return (
    <div className="container">
      <div className="loading">
        <ClipLoader color="#40BFFF" />
      </div>
    </div>
  )
}

export default Loading
