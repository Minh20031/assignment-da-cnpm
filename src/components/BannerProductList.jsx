import React from 'react'
import imgBanner from '../assets/image/Banner.png'
import { styled } from '@mui/system'
import { Box } from '@mui/material'

const Img = styled('img')({
  margin: '0',
  display: 'block',
  maxWidth: '100%',
  maxHeight: '100%',
})

function BannerProductList() {
	return (
		<Box sx={{width:'100%'}}>
				<Img
					alt="Banner" src={imgBanner} sx={{width:'100%'}}
				/>
		</Box>
	)
}

export default BannerProductList
