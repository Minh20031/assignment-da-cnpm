import React from 'react'
import { Pagination} from '@mui/material'

const PaginationComponent = (props) => {

  return (
    <Pagination 
      onChange={(e,page) => props.onPaginating(page)}
      defaultPage={props.page || 1}
      shape={props.shape||"circular"} 
      count={props.count}
      color={props.color||"secondaryColor"}
    />
  )
}

export default PaginationComponent
