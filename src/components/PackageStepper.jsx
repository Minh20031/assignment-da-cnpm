import React from "react";

import { Stepper } from "@mui/material";
import { Step } from "@mui/material";
import StepLabel from "@mui/material/StepLabel";
import { Box } from "@mui/material";

const steps = [
  "Đang đợi xác nhận",
  "Đang đóng gói",
  "Đang được vận chuyển",
  "Thành công",
];

export default function PackageStepper(props) {
  return (
    <Box sx={{ width: "100%" }}>
      <Stepper activeStep={props.step} alternativeLabel>
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
    </Box>
  );
}
