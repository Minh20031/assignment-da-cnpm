import React, {useState} from 'react'
import {Box, Typography} from '@mui/material'

function SidebarProductListCard(props) {
	const items = props.items;
	const title = props.title;
	const [idx, setIdx] = useState(props.idx); 
	return (
		<Box>
			<Box fullWidth sx={{backgroundColor: 'grayColor.main',p:2}}>
			<Box sx={{fontWeight:500, fontSize: 20, mb: 2}}>{title}</Box>
				{items.map((item,index)=>{
					return (
					<Box key={index} fullWidth 
						onClick={()=>{setIdx(index); props.handleOnClick(item.name);}}
						sx={{
							display: 'flex', justifyContent: 'space-between', mt: 1.5,
							color: idx===index? 'secondaryColor.main':'none',
							cursor: 'pointer'
						}}
					>	
						<Typography noWrap sx={{fontSize: 18, fontWeight:500, maxWidth: '70%', display:'inline-block'}}>{item.name}</Typography>
						<Box sx={{fontSize: 18,}}>{item.amount}</Box>
					</Box>
					)
				})}
			</Box>
		</Box>
	)
}

export default SidebarProductListCard
