import React from 'react'

import { useHistory } from 'react-router-dom'

import logo from '../assets/image/logo.png'

import { Typography } from '@mui/material'

const WelcomeComponent = (props) => {
  const history = useHistory();

  const handleClickLogo = () => {
    history.push('/')
  }
  return (
    <>
      <img
        src={logo}
        alt="Logo"
        style={{
          maxWidth: '200px',
          display: 'block',
          margin: '10px auto',
          cursor: 'pointer'
        }}
        onClick={handleClickLogo}
      />
      <Typography variant="h3" sx={{ mt: 2 }} gutterBottom align="center">
        Welcome to Book Soul
      </Typography>
      <Typography
        variant="h5"
        align="center"
        component="h4"
        sx={{ color: 'textColor.main', mt: 4 }}
      >
        {props.isLogin
          ? 'Đăng nhập để tiếp tục'
          : 'Hãy tạo một tài khoản riêng dành cho bạn'}
      </Typography>
    </>
  )
}

export default WelcomeComponent
