import React from 'react'
import { styled } from '@mui/system'
import logo from "../assets/image/white-logo.png";
import { AppBar, Toolbar, Box, ThemeProvider, IconButton, Typography, Button } from '@mui/material'
import {theme} from '../config'
import ArrowBackIosRoundedIcon from '@mui/icons-material/ArrowBackIosRounded';


const Img = styled('img')({
  margin: 'auto',
})

function HeaderbarManager() {
	return (
		<Box>
			<ThemeProvider theme={theme}>
				<AppBar position='static' color='primaryColor'>
					<Toolbar>
						<IconButton>
							<ArrowBackIosRoundedIcon color='whiteColor'/>
						</IconButton>
						<IconButton>
							<Img src={logo} width={45} height={45} edge="start"/>
						</IconButton>
						<Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            	Book Soul
          	</Typography>
						<Button sx={{color:'whiteColor.main'}}>
							<ArrowBackIosRoundedIcon color='whiteColor.main'/>
							<Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
								Đăng xuất
							</Typography>
						</Button>
					</Toolbar>
				</AppBar>
			</ThemeProvider>
		</Box>
	)
}

export default HeaderbarManager
