import React from 'react';
import { Grid } from '@mui/material';
import ModalInput from './ModalInput';
import card from '../assets/image/CreditCard.png';



const ModalContent2 = () => {
    return (
        <Grid container spacing={4}>
            <Grid item xs={12} sm={12} md={6}>
                <img src={card} alt="Card" style={{
                    width: "100%",
                }}/>
            </Grid>
            <Grid item xs={12} sm={12} md={6} container spacing={2}>
                <Grid item xs={12} sm={12} md={12}>
                    <ModalInput label={"Mã thẻ"} select={false}/>
                </Grid>
                <Grid item xs={6} sm={6} md={6}>
                    <ModalInput label={"Hạn sử dụng"} select={false}/>
                </Grid>
                <Grid item xs={6} sm={6} md={6}>
                    <ModalInput label={"CVV"} select={false}/>
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                    <ModalInput label={"Họ và tên chủ thẻ"} select={false}/>
                </Grid>
            </Grid>
            
        </Grid>
    );
}

export default ModalContent2

