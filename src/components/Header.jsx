import React from 'react'

import { useHistory,useLocation } from 'react-router-dom'

import { colors } from '../constants'
import { AppBar, Grid } from '@mui/material'
import Menu from '@mui/material/Menu'
import Paper from '@mui/material/Paper'
import InputBase from '@mui/material/InputBase'

import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined'
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined'
import SearchIcon from '@mui/icons-material/Search'
import CardGiftcardOutlinedIcon from '@mui/icons-material/CardGiftcardOutlined'
import LockOutlinedIcon from '@mui/icons-material/LockOutlined'
import ArrowBackIosNewOutlinedIcon from '@mui/icons-material/ArrowBackIosNewOutlined'
import AccountBoxOutlinedIcon from '@mui/icons-material/AccountBoxOutlined'

import ButtonComponent from './ButtonComponent'

import AuthenticateController from '../controller/AuthenticateController'


const Header = (props) => {
  const user = AuthenticateController.getUser();
  const isBig = window.innerWidth > 400;
  const text = (user && user.isManager ? "Quản lý/Đăng xuất" : "Tài khoản");
  const history = useHistory()
  const location = useLocation()
  const [anchorEl, setAnchorEl] = React.useState(null)
  const open = Boolean(anchorEl)
  const handleClick = (event) => {
    if (!user.isManager) setAnchorEl(event.currentTarget)
    else 
    {
      if (!location.pathname.includes("admin"))
      {
        history.push('/admin/product');
      }
      else 
      {
        handleLogOut();
      }
      
    }
  }
  const handleClose = () => {
    setAnchorEl(null)
  }

  const handleLinkToAuthPage = () => {
    history.push('/auth')
  }

  const handleLogOut = () => {
    AuthenticateController.signOut()
    history.push('/')
    props.setIsLogin(false)
  }
  const handleLinkToUserProfile = () => {
    history.push('/user/profile')
  }
  const handleLinkToUserPassword = () => {
    history.push('/user/password')
  }
  const handleLinkToUserOrder = () => {
    history.push('/user/order')
  }
  const handleLinkToCart = () => {
    history.push('/cart')
  }
  
  return (
    <AppBar
      position="static"
      sx={{
        background: colors[3],
        minHeight: '15px',
        padding: '10px',
        color: colors[3],
        // marginRight: '100px',
      }}
    >
      <Grid
        container
        spacing={0}
        style={{ gap: 30 }}
        direction="row"
        alignItems="center"
        justifyContent="flex-end"
      >
        
        <Grid >
          {' '}
          {props.isLogin ? (
            <Grid
              container
              spacing={0}
              style={{ gap: 10 }}
              direction="row"
              alignItems="flex-center"
              justifyContent="space-between"
            >
              <Grid>                
                  <ButtonComponent
                    isIcon={isBig ? false : true}
                    variant="contained"
                    children={isBig ? <span>{text}</span> : <AccountCircleOutlinedIcon color="primaryColor" />}
                    color="whiteColor"
                    size="small"
                    startIcon={
                      isBig ?<AccountCircleOutlinedIcon color="primaryColor" /> : <></>
                    }
                    sx={{ boxShadow: 0 }}
                    onClick={handleClick}
                  ></ButtonComponent>                
                <Menu
                  anchorEl={anchorEl}
                  open={open}
                  onClose={handleClose}
                  onClick={handleClose}
                >
                  <ButtonComponent
                    isIcon={false}
                    variant="contained"
                    children={<span>Thông tin tài khoản</span>}
                    color="whiteColor"
                    fullWidth={true}
                    size="medium"
                    startIcon={
                      <AccountBoxOutlinedIcon></AccountBoxOutlinedIcon>
                    }
                    sx={{
                      justifyContent: 'flex-start',
                      boxShadow: 0,
                      borderRadius: 0,
                    }}
                    onClick={handleLinkToUserProfile}
                  ></ButtonComponent>

                  <ButtonComponent
                    isIcon={false}
                    variant="contained"
                    children={<span>Đơn hàng</span>}
                    color="whiteColor"
                    fullWidth={true}
                    size="medium"
                    startIcon={
                      <CardGiftcardOutlinedIcon></CardGiftcardOutlinedIcon>
                    }
                    sx={{
                      justifyContent: 'flex-start',
                      boxShadow: 0,
                      borderRadius: 0,
                    }}
                    onClick={handleLinkToUserOrder}
                  ></ButtonComponent>

                  <ButtonComponent
                    isIcon={false}
                    variant="contained"
                    children={<span>Đổi mật khẩu</span>}
                    color="whiteColor"
                    fullWidth={true}
                    size="medium"
                    startIcon={<LockOutlinedIcon></LockOutlinedIcon>}
                    sx={{
                      justifyContent: 'flex-start',
                      boxShadow: 0,
                      borderRadius: 0,
                    }}
                    onClick={handleLinkToUserPassword}
                  ></ButtonComponent>
                  <ButtonComponent
                    isIcon={false}
                    variant="contained"
                    children={<span>Đăng xuất</span>}
                    color="whiteColor"
                    fullWidth={true}
                    size="medium"
                    startIcon={
                      <ArrowBackIosNewOutlinedIcon></ArrowBackIosNewOutlinedIcon>
                    }
                    sx={{
                      justifyContent: 'flex-start',
                      boxShadow: 0,
                      borderRadius: 0,
                    }}
                    onClick={handleLogOut}
                  ></ButtonComponent>
                </Menu>
              </Grid>

              <Grid>
                <ButtonComponent
                  isIcon={true}
                  variant="contained"
                  children={<ShoppingCartOutlinedIcon />}
                  color="primaryColor"
                  size="small"
                  onClick={handleLinkToCart}
                ></ButtonComponent>
              </Grid>
            </Grid>
          ) : (       
              
              <ButtonComponent
                isIcon={isBig ? false : true}
                variant="contained"
                children={isBig ? <span>Đăng nhập/Đăng ký</span> : <AccountCircleOutlinedIcon color="primaryColor" />}
                color="whiteColor"
                size="small"
                startIcon={
                  isBig ?<AccountCircleOutlinedIcon color="primaryColor" /> : <></>
                }
                sx={{
                      '&:hover': { boxShadow: 0, color: 'primaryColor.main' },
                      boxShadow: 0,
                    }}
                onClick={handleLinkToAuthPage}
              ></ButtonComponent>  
          )}
        </Grid>        
        
        <Grid>
          <Paper
            component="form"
            sx={{
              // p: '2px 4px',
              display: 'flex',
              alignItems: 'right',

              minWidth: 100,
              maxWidth: 180
            }}
          >
            <InputBase
              sx={{ ml: 1, flex: 1 }}
              placeholder="Tìm kiếm"
              inputProps={{ 'aria-label': 'search' }}
            />
            <ButtonComponent
              isIcon={true}
              variant="contained"
              children={<SearchIcon />}
              color="primaryColor"
              size="small"
            ></ButtonComponent>
          </Paper>
        </Grid>
      </Grid>
    </AppBar>
  )
}

export default Header
