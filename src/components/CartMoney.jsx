import React from 'react'
import { colors } from '../constants';
import { Box, TableRow, TableFooter, TableContainer, TableCell, TableBody, Table } from '@mui/material';
import ButtonComponent from './ButtonComponent';



const CartMoney = (props) => {

    return (
        <Box sx={{
            maxWidth: "300px",
            marginTop: "80px",
            display: "block",
            width: "100%"
        }}>
            <TableContainer>
                <Table>
                    <TableBody>
                        <TableRow
                            key={1}
                            sx={{
                                height: "40px",
                                '> td': {
                                    border: 0,
                                    padding: 0
                                }
                            }}
                        >
                            <TableCell align="left">Sản phẩm</TableCell>
                            <TableCell align="right">{'$' + props.productMoney.toFixed(2)}</TableCell>
                        </TableRow>
                        <TableRow
                            key={2}
                            sx={{ 
                                // '&:last-child td, &:last-child th': { border: 0 },
                                height: "40px",
                                '> td': {
                                    border: 0,
                                    padding: 0
                                }
                            }}
                        >
                            <TableCell align="left">Vận chuyển</TableCell>
                            <TableCell align="right">{"$" + props.deliveryMoney.toFixed(2)}</TableCell>
                        </TableRow>
                    </TableBody>
                    <TableFooter>
                    <TableRow
                            key={1}
                            sx={{ 
                                // '&:last-child td, &:last-child th': { border: 0 },
                                height: "60px",
                                '> td': {
                                    border: 0,
                                    padding: 0,
                                    borderTop: 1,
                                    borderColor: colors[5],
                                    color: colors[6],
                                    fontSize: 18,
                                    fontWeight: 'bold',
                                }
                            }}
                        >
                            <TableCell align="left">Tổng cộng</TableCell>
                            <TableCell align="right">{"$" + (props.deliveryMoney + props.productMoney).toFixed(2)}</TableCell>
                        </TableRow>
                    </TableFooter>
                </Table>
            </TableContainer>
            <ButtonComponent 
                isIcon={false}
                color={"primaryColor"}
                variant={"contained"}
                size={"large"}
                fullWidth={true}
                onClick={() => {
                    props.onClick()
                }}
            >
                Đặt hàng
            </ButtonComponent>
        </Box>
    );
}

export default CartMoney