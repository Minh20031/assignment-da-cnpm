export default class Comment {
    readonly commentId: string;
    readonly owner: string;
    readonly date: Date;
    readonly vote: 0 | 1 | 2 | 3 | 4 | 5;
    readonly content: string;
    readonly bookId: string;

    constructor(
        commentId: string,
        owner: string,
        date: Date,
        vote: 0 | 1 | 2 | 3 | 4 | 5,
        content: string,
        bookId: string,
    ) {
        this.commentId = commentId;
        this.owner = owner;
        this.date = date;
        this.vote = vote;
        this.content = content;
        this.bookId = bookId
    }
}