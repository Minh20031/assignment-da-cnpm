export type Publisher = 'Nhã Nam' | 'Alphabook' | 'Kim Đồng' | 'Kim Biên' | 'Giáo Dục' | 'Thời Đại' | 'Thanh Niên';
export type Genre     = 'Self-help' | 'Giáo dục' | 'Văn học thế giới' | 'Văn học nước nhà';

export default class Book {
    readonly bookId: string;
    readonly name: string;
    readonly quantity: number;
    readonly price: number; 
    readonly publisher: Publisher; 
    readonly genre: Genre;
    readonly createDate: Date;
    readonly image: string;
    readonly description: string;

    constructor(
        bookId: string,
        name: string,
        quantity: number,
        price: number,
        publisher: Publisher,
        genre: Genre,
        createDate: Date,
        image: string,
        description: string,
    ) {
        this.bookId = bookId;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.publisher = publisher;
        this.genre = genre;
        this.createDate = createDate;
        this.image = image;
        this.description = description;
    }
}