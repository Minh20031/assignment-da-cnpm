import Book from "./Book";

type Mutable<T> = {
    -readonly [k in keyof T]: T[k];
};

type OrderStatus = 'Waiting' | 'Proccessing' | 'Arriving' | 'Success' | 'Cancel'
export default class Order {
    readonly orderId: string;
    readonly userId: string;
    readonly books: {book: string, amount: number}[];
    readonly productMoney: number;
    readonly totalMoney: number;
    readonly createDate: Date;
    readonly status: OrderStatus;
    readonly paid: boolean;
    readonly province: string;
    readonly district: string;
    readonly wards: string;
    readonly address: string;
    readonly phone: string;

    constructor(
        orderId: string,
        userId: string,
        books: {book: string, amount: number}[],
        productMoney: number,
        totalMoney: number,
        createDate: Date,
        status: OrderStatus,
        paid: boolean,
        province: string,
        district: string,
        wards: string,
        address: string,
        phone: string,
    ) {
        this.orderId = orderId;
        this.userId = userId;
        this.books = books;
        this.productMoney = productMoney;
        this.totalMoney = totalMoney;
        this.createDate = createDate;
        this.status = status;
        this.paid = paid;
        this.province = province;
        this.district = district;
        this.wards = wards;
        this.address = address;
        this.phone = phone;
    }

    setStatus(val: OrderStatus) {
        const mutableThis = this as Mutable<Order>;
        mutableThis.status = val;
    }

    static createOrder(
        books: {book: Book, amount: number}[], 
        userId: string, 
        phone: string,
        shipFee: number, 
        paid: boolean,
        province: string,
        district: string,
        wards: string,
        address: string,
    ): Order {
        var productMoney = 0
        var newBooks = books.map((ele) => {
            productMoney += ele.book.price * ele.amount;
            return {
                book: ele.book.bookId,
                amount: ele.amount,
            }
        })
        return new Order(
            "",
            userId,
            newBooks,
            productMoney,
            productMoney + shipFee,
            new Date(),
            'Waiting',
            paid,
            province,
            district,
            wards,
            address,
            phone
        )
    }

    getProductAmount() {
        let sum = 0;
        this.books.forEach((ele) => {
            sum += ele.amount;
        })
        return sum
    }

    getBookList() {
        return this.books.map((ele) => {
            return ele.book
        })
    }
}
