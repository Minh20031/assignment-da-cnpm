export default class User {
    readonly userId: string;
    readonly username: string;
    readonly email: string;
    readonly isManager: boolean;
    readonly avt: string;
    readonly province: string;
    readonly district: string;
    readonly wards: string;
    readonly address: string;
    readonly phone: string;
    readonly isVerified: boolean;

    constructor(
        userId: string,
        username: string,
        email: string,
        isManager: boolean,
        avt: string,
        province: string,
        district: string,
        wards: string,
        address: string,
        phone: string,
        isVerified: boolean
    ) {
        this.userId = userId;
        this.username = username;
        this.email = email;
        this.isManager = isManager;
        this.avt = avt;
        this.province = province;
        this.district = district;
        this.wards = wards;
        this.address = address;
        this.phone = phone;
        this.isVerified = isVerified;
    }
}