// import React, { useState } from "react";
import UserSidebar from "../../components/UserSidebar";
import FormUserComponent from "../../components/FormUserComponent";
import { Grid } from "@mui/material";

const UserProfilePage = () => {
  return (
    <div>
      <Grid container spacing={2}>
        <Grid item xs={4}>
          <UserSidebar index={0} />
        </Grid>
        <Grid item xs={8}>
          <FormUserComponent />
        </Grid>
      </Grid>
    </div>
  );
};

export default UserProfilePage;
