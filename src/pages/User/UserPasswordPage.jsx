import React, { useState } from "react";
import FormPasswordComponent from "../../components/FormPasswordComponent";
import { Grid } from "@mui/material";
import UserSidebar from "../../components/UserSidebar";
import { Typography } from "@mui/material";
import { useHistory } from "react-router-dom";
import AuthenticateController from "../../controller/AuthenticateController";

const initialForm = {
  oldPassword: "",
  newPassword: "",
  confirmPassword: "",
};

const UserPasswordPage = () => {
  const history = useHistory();
  // let user = AuthenticateController.getUser();
  const [form, setForm] = useState(initialForm);
  const [error, setError] = useState("");

  const handleChangeForm = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };
  const handleSubmitForm = async (e) => {
    e.preventDefault();

    if (form.confirmPassword !== form.newPassword) {
      setError("Mật khẩu xác nhận không đúng");
      return;
    }
    const ans = await AuthenticateController.changePassword(
      form.oldPassword,
      form.newPassword
    );
    if (ans) {
      await AuthenticateController.signOut();
      history.push("/auth");
    } else setError("Connection failed!");
  };

  return (
    <div>
      <Grid container spacing={2}>
        <Grid item xs={4}>
          <UserSidebar index={2} />
        </Grid>
        <Grid item xs={8}>
          <FormPasswordComponent
            initialForm={initialForm}
            handleChangeForm={handleChangeForm}
            handleSubmitForm={handleSubmitForm}
          />
          <Typography color="dangerColor.main" sx={{ mb: 2 }}>
            {error}
          </Typography>
        </Grid>
      </Grid>
    </div>
  );
};

export default UserPasswordPage;
