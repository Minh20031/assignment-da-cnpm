// import React from "react";
import OrderTrackingListComponent from '../../components/OrderTrackingListComponent'
import OrderPayment from '../../components/OrderPayment'
import { Grid, Box, Typography } from '@mui/material'
import UserSidebar from '../../components/UserSidebar'
import { useEffect, useRef, useState } from 'react'
import LoadingComponent from '../../components/LoadingComponent'
import AuthenticateController from '../../controller/AuthenticateController'
import OrderController from '../../controller/OrderController'
import { colors } from '../../constants/index'
import ProductItem from '../../components/ProductItem'
import CustomizedSteppers from '../../components/CustomizedSteppers'
import ButtonComponent from '../../components/ButtonComponent'
import ArrowBackRoundedIcon from '@mui/icons-material/ArrowBackRounded'
import ProductController from '../../controller/ProductController'

const UserOrderPage = () => {
  const [orders, setOrders] = useState([])
  const [select, setSelect] = useState(-1)
  const [products, setProducts] = useState(null)
  const isUnmounted = useRef(false);

  useEffect(() => {
    async function handler() {
      let user = AuthenticateController.getUser()
      if (user !== null) {
        let data = await OrderController.loadUserOrders(user.userId)
        setOrders(data)
      } else {
        setOrders([])
      }
    }

    handler()
  }, [])

  useEffect(() => {
    return () => {
      isUnmounted.current = true;
    }
  }, [])

  useEffect(() => {
    async function handler() {
      if (select > -1) {
        let data = await ProductController.loadBooksFromIds(
          orders[select].getBookList()
        )
        setProducts(data)
      } else {
        setProducts(null)
      }
    }
    handler()
  }, [select, orders])

  return (
    <div>
      <Grid container spacing={2}>
        <Grid item xs={4}>
          <UserSidebar index={1} />
        </Grid>
        <Grid item xs={8}>
          {orders === [] ? (
            <Box
              sx={{
                width: '100%',
                height: '100%',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <LoadingComponent />
            </Box>
          ) : select === -1 ? (
            <OrderTrackingListComponent
              data={orders}
              onItemClick={(index) => {
                setSelect(index)
              }}
            />
          ) : (
            <Box>
              <Box
                sx={{
                  width: '100%',
                  display: 'flex',
                  alignItems: 'center',
                  fontSize: '22px',
                  fontWeight: '500',
                }}
              >
                <ButtonComponent
                  isIcon={true}
                  color={'secondaryColor'}
                  variant={'contained'}
                  size={'large'}
                  onClick={() => {
                    setSelect(-1)
                  }}
                >
                  <ArrowBackRoundedIcon />
                </ButtonComponent>
                Back
              </Box>
              <CustomizedSteppers
                step={(() => {
                  switch (orders[select].status) {
                    case 'Waiting':
                      return 0
                    case 'Processing':
                      return 1
                    case 'Arriving':
                      return 2
                    case 'Success':
                      return 3
                    default:
                      return 0
                  }
                })()}
                steps={['Waiting', 'Processing', 'Arriving', 'Success']}
              />
              <Grid
                container
                spacing={1}
                sx={{
                  marginTop: '20px',
                }}
              >
                <Grid item xs={12} sm={12} md={6}>
                  <Box
                    sx={{
                      display: 'flex',
                      width: '100%',
                      flexDirection: 'column',
                      alignItems: 'center',
                    }}
                  >
                    <OrderPayment
                      numberOfItems={orders[select].getProductAmount()}
                      cost={orders[select].totalMoney}
                      shippingFee={
                        orders[select].totalMoney - orders[select].productMoney
                      }
                      paymentState={orders[select].status}
                    />
                  </Box>
                </Grid>
                <Grid item xs={12} sm={12} md={6}>
                  <Box
                    sx={{
                      display: 'flex',
                      width: '100%',
                      flexDirection: 'column',
                      alignItems: 'center',
                    }}
                  >
                    <Typography
                      sx={{
                        fontSize: '14px',
                        fontWeight: 'bold',
                        color: colors[8],
                        width: 300,
                      }}
                    >
                      Sản phẩm
                    </Typography>
                    <Box
                      sx={{
                        width: 300,
                        height: 350,
                        backgroundColor: 'grayColor',
                        overflowY: 'overlay',
                      }}
                    >
                      {orders[select].books.map((ele, index) => {
                        if (products === null) return undefined
                        var book = products[ele.book]
                        if (book)
                          return (
                            <Grid
                              key={index}
                              container
                              item
                              xs={12}
                              direction="column"
                            >
                              <ProductItem
                                product={{
                                  img: book.image,
                                  bookName: book.name,
                                  bookCost: book.price,
                                }}
                              ></ProductItem>
                            </Grid>
                          )
                        else return undefined
                      })}
                    </Box>
                    {orders[select].status === "Arriving" ?
                      <ButtonComponent 
                        isIcon={false}
                        color={"primaryColor"}
                        variant={"contained"}
                        size={"large"}
                        fullWidth={true}
                        onClick={async () => {
                          var res = await OrderController.updateOrderStatus(orders[select].orderId, "Success");
                          if (res && !isUnmounted.current) {
                            orders[select].setStatus("Success");
                            let newOrder = [...orders]
                            setOrders(newOrder)
                          }
                        }}
                      >
                        Xác nhận
                      </ButtonComponent> :
                      null
                    }
                  </Box>
                </Grid>
              </Grid>
            </Box>
          )}
        </Grid>
      </Grid>
    </div>
  )
}

export default UserOrderPage
