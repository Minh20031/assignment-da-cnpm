import React, { useEffect, useState } from 'react'
import BookItem from '../components/BookItem'
import PaginationComponent from '../components/PaginationComponent'
import SidebarProductList from '../components/SidebarProductList'
import BannerProductList from '../components/BannerProductList'
import FilterBarProductList from '../components/FilterBarProductList'
import { Box, Grid, Drawer } from '@mui/material'
import ProductController from '../controller/ProductController'
import Loading from '../components/LoadingComponent'
// import { ConstructionOutlined } from '@mui/icons-material'
// import { SubscriptionsOutlined } from '@mui/icons-material'

// import AuthenticateController from "../controller/AuthenticateController";

const ProductListPage = () => {
  const [loading, setLoading] = useState(true)
  const [all_products, setAll_products] = useState([])
  const [displayProds, setDisplayProds] = useState([])
  const [onPageProds, setOnPageProds] = useState([])

  const [drawerOpen, setDrawerOpen] = useState(false)
  const [sortBy, setSortBy] = useState('Tên')

  const [publisher, setPublisher] = useState('All')
  const [genre, setGenre] = useState('All')
  const [price, setPrice] = useState([0, 10000])
  const [page, setPage] = useState(1)

  const [publishers, setPublishers] = useState({
    All: 0,
    'Nhã nam': 0,
    Alphabook: 0,
    'Kim đồng': 0,
    'NXB Giáo dục': 0,
    'NXB Thời đại': 0,
    'NXB Thanh niên thien nanh': 0,
  })
  const [genres, setGenres] = useState({
    All: 0,
    'Self-help': 0,
    'Giáo dục': 0,
    'Văn học Việt Nam': 0,
    'Văn học thế giới': 0,
  })

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true)
      const all_prods = (await ProductController.loadBooks()).filter(ele => ele.quantity > 0);
      // console.log(all_prods)

      // process for sidebar
      var tmp_publishers = {
        All: 0,
        'Nhã Nam': 0,
        'Alphabook': 0,
        'Kim Đồng': 0,
        'Kim Biên': 0,
        'Giáo Dục': 0,
        'Thời Đại': 0,
        'Thanh Niên': 0,
      }
      var tmp_genres = {
        All: 0,
        'Self-help': 0,
        'Giáo dục': 0,
        'Văn học nước nhà': 0,
        'Văn học thế giới': 0,
      }
      all_prods.map((prod) => {
        if (prod['publisher'] in tmp_publishers) {
          tmp_publishers[prod['publisher']] += 1
          tmp_publishers['All'] += 1
        }
        if (prod['genre'] in tmp_genres) {
          tmp_genres[prod['genre']] += 1
          tmp_genres['All'] += 1
        }
        return 0
      })
      setPublishers(tmp_publishers)
      setGenres(tmp_genres)

      //////
      setAll_products(all_prods)
      setDisplayProds(all_prods)
      setOnPageProds(all_prods.slice(0, Math.min(12, all_prods.length)))
      setLoading(false)
    }
    fetchData()
  }, [])

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: 'smooth' })
  }, [])

  useEffect(() => {
    var tmpProds = displayProds
    if (sortBy === 'Tên') {
      tmpProds.sort((a, b) => (a['name'] < b['name'] ? -1 : 1))
      // console.log(sortBy)
      // console.log(tmpProds)
    } else if (sortBy === 'Giá tiền') {
      tmpProds.sort((a, b) => a['price'] - b['price'])
    }
    setDisplayProds(tmpProds)
    setPage(1)
    setOnPageProds(tmpProds.slice(0, Math.min(12, tmpProds.length)))
  }, [sortBy, displayProds])

  if (loading) {
    return (
      <Grid container justifyContent="center" alignItems="center">
        <Grid item>
          <Loading />
        </Grid>
      </Grid>
    )
  }

  const toggleDrawer = (open) => (event) => {
    if (
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return
    }
    setDrawerOpen(open)
  }

  const handlePaginating = (page) => {
    setOnPageProds(
      displayProds.slice(
        (page - 1) * 12,
        Math.min(displayProds.length, page * 12)
      )
    )
  }

  const handleOnChangePublisher = (e) => {
    // console.log(e);
    setPublisher(e)
    handleFilter(e, genre, price)
  }

  const handleOnChangeGenre = (e) => {
    // console.log(e);
    setGenre(e)
    handleFilter(publisher, e, price)
  }

  const handleOnChangePrice = (price) => {
    // console.log(price);
    setPrice(price)
  }

  const handleFilter = (pub, gen, pri) => {
    // console.log('filtering');
    // console.log(pub);
    // console.log(gen);
    // console.log(pri);

    const newDisplayProds = all_products.filter(
      (item) =>
        (item.publisher === pub || pub === 'All') &&
        (item.genre === gen || gen === 'All') &&
        parseFloat(item.price) >= pri[0] &&
        parseFloat(item.price) <= pri[1]
    )

    setDisplayProds(newDisplayProds)
    setPage(1)
    setOnPageProds(
      newDisplayProds.slice(0, Math.min(newDisplayProds.length, 12))
    )

    // console.log(newDisplayProds);
    // console.log(newDisplayProds.slice(0, Math.min(newDisplayProds.length,12)));
  }

  const handleOnClickFilter = () => {
    // console.log('click filter');
    handleFilter(publisher, genre, price)
  }

  return (
    <div>
      <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
        <Box id="sidebar-product-list">
          <Drawer open={drawerOpen} onClose={toggleDrawer(false)}>
            <SidebarProductList
              onChangePublisher={handleOnChangePublisher}
              onChangeGenre={handleOnChangeGenre}
              onChangePrice={handleOnChangePrice}
              handleOnClickFilter={handleOnClickFilter}
              publishers={publishers}
              genres={genres}
            />
          </Drawer>
          <SidebarProductList
            onChangePublisher={handleOnChangePublisher}
            onChangeGenre={handleOnChangeGenre}
            onChangePrice={handleOnChangePrice}
            handleOnClickFilter={handleOnClickFilter}
            publishers={publishers}
            genres={genres}
            sx={{ display: { xs: 'none', lg: 'block' } }}
          />
        </Box>

        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            pl: { xs: 1, lg: 6 },
            alignItems: 'center',
            width: '100%',
          }}
        >
          <BannerProductList />
          <Box sx={{ mb: 7, width: '100%' }}>
            <FilterBarProductList
              numberProducts={displayProds.length}
              toggleDrawer={toggleDrawer}
              setSortBy={setSortBy}
            />
          </Box>
          {onPageProds.length === 0 ? (
            <Box sx={{ mb: 3 }}>Không tìm thấy sản phẩm</Box>
          ) : (
            <Grid container spacing={2} sx={{ mb: 7 }}>
              {onPageProds.map((product, index) => {
                return (
                  <Grid key={index} item xs={12} sm={6} md={4} lg={4} xl={3}>
                    <BookItem
                      sx={{
                        width: '100%',
                        display: 'flex',
                        justifyContent: 'center',
                      }}
                      product={product}
                    />
                  </Grid>
                )
              })}
            </Grid>
          )}

          <Box>
            {onPageProds.length === 0 ? (
              <></>
            ) : (
              <PaginationComponent
                count={
                  displayProds.length === 0
                    ? 0
                    : Math.ceil(displayProds.length / 12)
                }
                shape="rounded"
                color="secondaryColor"
                page={page}
                onPaginating={handlePaginating}
              />
            )}
          </Box>
        </Box>
      </Box>
    </div>
  )
}

export default ProductListPage
