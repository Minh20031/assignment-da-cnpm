import React, { useState, useEffect, useRef } from 'react'

import { useHistory } from 'react-router-dom'
import TableAdminOrder from '../../components/TableAdminOrder'
import { Grid } from '@mui/material'
import OrderController from '../../controller/OrderController'
import Loading from '../../components/LoadingComponent'
import PaginationComponent from '../../components/PaginationComponent'

import AuthenticateController from '../../controller/AuthenticateController'

const numberOfElementsInPage = 10
const AdminOrderPage = () => {
  const history = useHistory()
  const [loading, setLoading] = useState(true)
  const [displayProds, setDisplayProds] = useState([])
  const [onPageProds, setOnPageProds] = useState([])
  const user = AuthenticateController.getUser()

  const isUnmounted = useRef(false)
  useEffect(() => {
    return () => {
      isUnmounted.current = true
    }
  }, [])
  useEffect(() => {
    const fetchData = async () => {
      setLoading(true)
      const all_orders = await OrderController.loadOrders()
      if (!isUnmounted.current) {
        setDisplayProds(all_orders)
        setOnPageProds(
          all_orders.slice(
            0,
            Math.min(numberOfElementsInPage, all_orders.length)
          )
        )
        setLoading(false)
      }
    }
    fetchData()
  }, [])
  const handlePaginating = (page) => {
    setOnPageProds(
      displayProds.slice(
        (page - 1) * numberOfElementsInPage,
        Math.min(displayProds.length, page * numberOfElementsInPage)
      )
    )
  }
  if (loading) {
    return (
      <Grid container justifyContent="center" alignItems="center">
        <Grid item>
          <Loading />
        </Grid>
      </Grid>
    )
  } else if (!user) {
    history.push('/')
  } else if (!user.isManager) {
    history.push('/')
  }
  return (
    <Grid
      container
      // direction="column"
      justifyContent="space-between"
      alignItems="stretch"
    >
      <Grid item xs={12}>
        <TableAdminOrder data={onPageProds} />
      </Grid>
      <Grid item xs={12}>
        <Grid item container justifyContent="center">
          <Grid item>
            <PaginationComponent
              count={
                displayProds.length === 0
                  ? 0
                  : Math.ceil(displayProds.length / numberOfElementsInPage)
              }
              shape="rounded"
              color="secondaryColor"
              // page={page}
              onPaginating={handlePaginating}
            />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}

export default AdminOrderPage
