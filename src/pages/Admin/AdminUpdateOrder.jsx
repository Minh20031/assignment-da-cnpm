import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { Grid, Box, Typography } from '@mui/material'
import ButtonComponent from '../../components/ButtonComponent'

import AdminOrderDelivery from '../../components/AdminOrderDelivery'
import OrderPayment from '../../components/OrderPayment'
import ProductItem from '../../components/ProductItem'
import { colors } from '../../constants'
import OrderController from '../../controller/OrderController'
import Loading from '../../components/LoadingComponent'
import ProductController from '../../controller/ProductController'
import AuthenticateController from '../../controller/AuthenticateController'

//from AuthPage.jsx
const AdminUpdateOrder = (props) => {
  const history = useHistory()
  var orderId =
    window.location.href.split('/')[window.location.href.split('/').length - 1]
  const [order, setOrder] = useState(OrderController.loadOrder(orderId) || [])
  const [loading, setLoading] = useState(true)
  const user = AuthenticateController.getUser()

  const [status, setStatus] = useState(OrderController.loadOrder(orderId)?.status || '')
  const [books, setBooks] = useState(null);
  const handleClickConfirmWaiting = async () => {
    if (status === 'Waiting') {
      setStatus('Processing')
      await OrderController.updateOrderStatus(orderId, 'Processing')
    } else if (status === 'Processing') {
      setStatus('Arriving')
      await OrderController.updateOrderStatus(orderId, 'Arriving')
    } else {
      setStatus('Success')
      await OrderController.updateOrderStatus(orderId, 'Success')
    }
  }
  const handleClickCancel = async () => {
    setStatus('Cancel')
    await OrderController.updateOrderStatus(orderId, 'Cancel')
  }
  useEffect(() => {
    const fetchData = async () => {
      setLoading(true)

      const order = await OrderController.loadOrder(orderId);
      if (!order) return;

      let data = await ProductController.loadBooksFromIds(
        order.getBookList()
      )

      setBooks(data);
      setOrder(order)


      setStatus(order.status)
      setLoading(false)
    }
    fetchData()
  }, [orderId])
  if (loading) {
    return (
      <Grid container justifyContent="center" alignItems="center">
        <Grid item>
          <Loading />
        </Grid>
      </Grid>
    )
  }
  else if (!user) {
    history.push('/')
  }
  else if (!user.isManager) {
    history.push('/')

  }
  return (
    <Grid container spacing={0} alignItems="flex-start" justifyContent="center">
      <Grid
        item

      // sx={{
      // minWidth: {
      //     lg: '50%',
      //     xs: '100%',
      // },
      // }}
      >
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <AdminOrderDelivery
              id={order.orderId}
              address={order.address}
              phoneNumber={order.phone}
              state={status}
            />
          </Grid>
          <Grid item xs={12}>
            <OrderPayment
              numberOfItems={order.books.length}
              cost={order.totalMoney}
              shippingFee={order.totalMoney - order.productMoney}
              paymentState={order.paid ? 'Đã thanh toán' : 'Chưa thanh toán'}
            />
          </Grid>
        </Grid>
      </Grid>

      <Grid
        item
      // sx={{
      // minWidth: {
      //     lg: '50%',
      //     xs: '100%',
      // },
      // }}
      >
        <Grid
          container
          spacing={3}
          flexDirection="column"
          justifyContent="center"
        // alignItems="center"
        >
          <Grid item xs={12} sx={{ ml: { lg: '20%', xs: '13%' } }}>
            <Typography
              sx={{
                fontSize: '16px',
                fontWeight: 'bold',
                color: colors[8],
                marginTop: '20px',
                marginBottom: '10px',
              }}
            >
              Sản phẩm
            </Typography>{' '}
          </Grid>
          <Grid item xs={12} sx={{ ml: { lg: '20%', xs: '13%' } }}>
            <Box
              sx={{
                width: 350,
                height: 350,
                backgroundColor: 'grayColor',
                overflowY: 'scroll',
              }}
            >
              <Grid container spacing={2} sx={{ mb: 7 }}>
                {order.books.map((e) => {
                  if (!books) return null;
                  var book = books[e.book]
                  return (
                    <Grid container item xs={12} direction="column" key={e.book} >
                      <ProductItem product={{
                        img: book.image,
                        bookName: book.name,
                        bookCost: book.price,
                      }}></ProductItem>
                    </Grid>
                  )
                })}
              </Grid>
            </Box>
          </Grid>

          {status === 'Waiting' ? (
            <Grid item xs={12} minHeight={100}>
              <Grid item sx={{ ml: { lg: '20%', xs: '13%' } }}>
                <ButtonComponent
                  sx={{ padding: '10px', width: '100%', my: 2 }}
                  variant="contained"
                  color="secondaryColor"
                  children={<span>Xác nhận đơn hàng</span>}
                  onClick={handleClickConfirmWaiting}
                />
              </Grid>
              <Grid item sx={{ ml: { lg: '20%', xs: '13%' } }}>
                <ButtonComponent
                  sx={{ padding: '10px', width: '100%', my: 2 }}
                  variant="contained"
                  color="dangerColor"
                  children={<span>Hủy đơn hàng</span>}
                  onClick={handleClickCancel}
                  display="none"
                />
              </Grid>
            </Grid>
          ) : (
            <div></div>
          )}
          {status === 'Processing' ? (
            <Grid item xs={12} minHeight={100}>
              <Grid item sx={{ ml: { lg: '20%', xs: '13%' } }}>
                <ButtonComponent
                  sx={{ padding: '10px', width: '100%', my: 2 }}
                  variant="contained"
                  color="secondaryColor"
                  children={<span>Đã giao đơn hàng</span>}
                  onClick={handleClickConfirmWaiting}
                />
              </Grid>
            </Grid>
          ) : (
            <div></div>
          )}
          {status === 'Cancel' || status === 'Arriving' ? (
            <Grid item xs={12} minHeight={100}></Grid>
          ) : (
            <div></div>
          )}
        </Grid>
      </Grid>
    </Grid>
  )
}

export default AdminUpdateOrder
