import React, { useState, useEffect } from 'react'

import { Grid, TextField, MenuItem } from '@mui/material'
import ButtonComponent from '../../components/ButtonComponent'
import logo from '../../assets/image/logo.png'
import { useHistory } from 'react-router-dom'
import ProductController from '../../controller/ProductController'
import Loading from '../../components/LoadingComponent'

import AuthenticateController from '../../controller/AuthenticateController'

const publishers = [
  {
    id: 0,
    publisher: 'Nhã Nam',
  },
  {
    id: 1,
    publisher: 'Alphabook',
  },
  {
    id: 2,
    publisher: 'Kim Đồng',
  },
  {
    id: 3,
    publisher: 'Kim Biên',
  },
  {
    id: 4,
    publisher: 'Giáo Dục',
  },
  {
    id: 5,
    publisher: 'Thời Đại',
  },
  {
    id: 6,
    publisher: 'Thanh Niên',
  },
]
const genres = [
  {
    id: 0,
    genre: 'Self-help',
  },
  {
    id: 1,
    genre: 'Giáo dục',
  },
  {
    id: 2,
    genre: 'Văn học thế giới',
  },
  {
    id: 3,
    genre: 'Văn học nước nhà',
  },
]
//from AuthPage.jsx
const AdminProductDetailPage = () => {
  const history = useHistory()
  const user = AuthenticateController.getUser()
  var bookId =
    window.location.href.split('/')[window.location.href.split('/').length - 1]
  const [book, setBook] = useState()
  const [loading, setLoading] = useState(true)

  const [image, setImage] = useState(undefined)
  const [name, setName] = useState()
  const [price, setPrice] = useState()
  const [quantity, setQuantity] = useState()
  const [publisher, setPublisher] = useState('')
  const [genre, setGenre] = useState('')
  const [description, setDescription] = useState('')

  const handleChangeName = (event) => {
    setName(event.target.value)
    book.name = event.target.value
    setBook(book)
  }
  const handleChangePrice = (event) => {
    setPrice(event.target.value)
    book.price = parseInt(event.target.value)
    setBook(book)
  }
  const handleChangeQuantity = (event) => {
    setQuantity(event.target.value)
    book.quantity = parseInt(event.target.value)
    setBook(book)
  }
  const handleChangePublisher = (event) => {
    setPublisher(event.target.value)
    book.publisher = event.target.value

    setBook(book)
  }
  const handleChangeGenre = (event) => {
    setGenre(event.target.value)
    book.genre = event.target.value
    setBook(book)
  }
  const handleChangeDescription = (event) => {
    setDescription(event.target.value)
    book.description = event.target.value
    setBook(book)
  }
  const handleClickSaveUpdate = () => {
    history.push('/admin/product')
      ; (async () => {
        if (bookId !== ':id') await ProductController.updateBookById(bookId, book)
        else await ProductController.uploadBook(book)
        // setImage(book.image)
      })()
  }
  useEffect(() => {
    const fetchData = async () => {
      setLoading(true)
      var book;
      if (bookId !== ":id") {
        book = await ProductController.loadBook(bookId);
        if (!book) return;
      }
      else book = {}
      setBook(book);
      setImage(book.image);
      setName(book.name);
      setPrice(book.price);
      setQuantity(book.quantity);
      setPublisher(book.publisher);
      setGenre(book.genre);
      setDescription(book.description);
      setLoading(false)
    }
    fetchData()
  }, [bookId])

  if (loading) {
    return (
      <Grid container justifyContent="center" alignItems="center">
        <Grid item>
          <Loading />
        </Grid>
      </Grid>
    )
  } else if (!user) {
    history.push('/')
  } else if (!user.isManager) {
    history.push('/')
  }
  return (
    <Grid container alignItems="center">
      <Grid
        item
        sx={{
          minWidth: {
            lg: '50%',
            xs: '100%',
          },
          marginBottom: '50px',
          // width: { lg: '50%' },
          // mb: 2,
          // display: {
          //     xs: 'none',
          //     lg: 'block'
          // },
        }}
      >
        <Grid item>
          <img
            src={image || logo}
            alt="Product"
            style={{
              maxWidth: '300px',
              display: 'block',
              margin: '10px auto',
              // onChange={handleChangeImage}
            }}
          />
          <div sx={{ mt: 2 }} align="center">
            <input id="input-image" type="file" accept={'image/*'} hidden
              onChange={async (event) => {
                var files = event.target.files
                if (files != null) {
                  if (files[0].type.split('/')[0] === 'image') {
                    var newImage = await ProductController.convertImageAsync(files[0])
                    setImage(newImage);
                    book.image = newImage;
                    // ProductController.uploadImage(newImage, bookId);
                  }
                }
              }}
            />
            <ButtonComponent
              // for = "file-input"
              justifyContent="center"
              isIcon={false}
              variant="contained"
              children={<span>Sửa ảnh</span>}
              color="primaryColor"
              onClick={() => {
                var element = document.getElementById("input-image");
                if (element != null) {
                  element.click();
                }
              }}
            ></ButtonComponent>
            {/* file-input {
                            display: none;
                            } */}
            {/* <input type="file" id="file-input" display="none"/> */}
          </div>
        </Grid>
      </Grid>

      <Grid
        item
        sx={{
          minWidth: {
            lg: '50%',
            xs: '100%',
          },
        }}
      >
        <Grid container spacing={2} flexDirection="column">
          <Grid item>
            <TextField
              required
              id="outlined-required"
              label="Tên sản phẩm"
              onChange={handleChangeName}
              value={name || ''}
              fullWidth
            />
          </Grid>

          <Grid item>
            <TextField
              required
              id="outlined-required"
              label="Giá"
              onChange={handleChangePrice}
              value={price || ''}
              fullWidth
            />
          </Grid>

          <Grid item>
            <TextField
              required
              id="outlined-required"
              label="Số lượng"
              onChange={handleChangeQuantity}
              value={quantity || ''}
              fullWidth
            />
          </Grid>

          <Grid item>
            <TextField
              fullWidth
              select
              label="Thể loại"
              defaultValue=""
              value={genre || ''}
              onChange={handleChangeGenre}
            >
              {genres.map((g) => (
                <MenuItem key={g.id} value={g.genre}>
                  {g.genre}
                </MenuItem>
              ))}
            </TextField>
          </Grid>

          <Grid item>
            <TextField
              fullWidth
              select
              label="Nhà xuất bản"
              defaultValue=""
              value={publisher || ''}
              onChange={handleChangePublisher}
            >
              {publishers.map((p) => (
                <MenuItem key={p.id} value={p.publisher}>
                  {p.publisher}
                </MenuItem>
              ))}
            </TextField>
          </Grid>

          <Grid item>
            <TextField
              label="Thông tin mô tả"
              multiline
              rows={4}
              onChange={handleChangeDescription}
              variant="outlined"
              value={description}
              fullWidth
            />
          </Grid>

          <Grid item sx={{ ml: { lg: '20%', xs: '13%' } }}>
            <ButtonComponent
              sx={{ padding: '10px', width: '80%', my: 2 }}
              variant="contained"
              color="primaryColor"
              children={<span>LƯU</span>}
              onClick={() => {
                handleClickSaveUpdate()
              }}
            />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}

export default AdminProductDetailPage
