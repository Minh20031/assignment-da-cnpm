import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'

import TableAdminProduct from '../../components/TableAdminProduct'
import PaginationComponent from '../../components/PaginationComponent'
import ProductController from '../../controller/ProductController'
import Loading from '../../components/LoadingComponent'
import { Box, Grid } from '@mui/material'

import AuthenticateController from '../../controller/AuthenticateController'

const numberOfElementsInPage = 10
const AdminProductPage = () => {
  // const [products, setProducts] = useState()
  const history = useHistory()
  const [loading, setLoading] = useState(true)
  const [displayProds, setDisplayProds] = useState([])
  const [onPageProds, setOnPageProds] = useState([])
  const user = AuthenticateController.getUser()
  
  const handleDeleteBook = async (id) => {
    const check = await ProductController.deleteBook(id);
    if(check) {
      const newData = onPageProds.filter(c => c.bookId !== id)
      const newAllBooks = displayProds.filter(c => c.bookId !== id)
      setDisplayProds(newAllBooks)
      setOnPageProds(
        newData
      )
    }
  }
  // const [page, setPage] = useState(1)
  useEffect(() => {
    const fetchData = async () => {
      setLoading(true)
      const all_books = await ProductController.loadBooks(null, null)
      // setProducts(all_books)
      setDisplayProds(all_books)
      setOnPageProds(
        all_books.slice(0, Math.min(numberOfElementsInPage, all_books.length))
      )
      setLoading(false)
    }
    fetchData()
  }, [])
  const handlePaginating = (page) => {
    setOnPageProds(
      displayProds.slice(
        (page - 1) * numberOfElementsInPage,
        Math.min(displayProds.length, page * numberOfElementsInPage)
      )
    )
  }
  if (loading) {
    return (
      <Grid container justifyContent="center" alignItems="center">
        <Grid item>
          <Loading />
        </Grid>
      </Grid>
    )
  } else if (!user) {
    history.push('/')
  } else if (!user.isManager) {
    history.push('/')
  }
  return (
    <div>
      <Box
        sx={{
          // display: 'flex',
          flexDirection: 'column',
          // pl: { xs: 1, lg: 6 },
          alignItems: 'center',
          // width: '100%'
        }}
      >
        <TableAdminProduct data={onPageProds} handleDeleteBook={handleDeleteBook}/>
        <Grid container justifyContent="center">
          <Grid item>
            <PaginationComponent
              count={
                displayProds.length === 0
                  ? 0
                  : Math.ceil(displayProds.length / numberOfElementsInPage)
              }
              shape="rounded"
              color="secondaryColor"
              // page={page}
              onPaginating={handlePaginating}
            />
          </Grid>
        </Grid>
      </Box>
    </div>
  )
}

export default AdminProductPage
