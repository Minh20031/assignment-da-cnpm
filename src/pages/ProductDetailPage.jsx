import React, { useState, useEffect } from 'react'

import { useDispatch } from 'react-redux'
import { useParams } from 'react-router-dom'

import { Grid, Box } from '@mui/material'

import ProductPanel from '../components/ProductPanel'
import ProductDetailContent from '../components/ProductDetailContent'
import ProductNewComment from '../components/ProductNewComment'
import Loading from '../components/LoadingComponent'

import { ADD_TO_CART } from '../constants'

import AuthenticateController from '../controller/AuthenticateController'
import ProductController from '../controller/ProductController'
import Comment from '../models/Comment'

const initialStateForComment = { content: '', value: 5 }

const ProductDetailPage = () => {
  const dispatch = useDispatch()
  const [loading, setLoading] = useState(true)
  const [newComment, setNewComment] = useState(initialStateForComment)
  const [amount, setAmount] = useState(1)
  const [comments, setComments] = useState([])
  const [product, setProduct] = useState()
  const [user, setUser] = useState()
  const { id } = useParams()

  const handleAddToCart = (book, quantity) => {
    dispatch({
      type: ADD_TO_CART,
      payload: { book, amount: quantity },
    })
  }
  const handleUpdateNewComment = (value, key) => {
    setNewComment({ ...newComment, [key]: value })
  }
  const handleCreateNewComment = async () => {
    const nComment = new Comment(
      '',
      user.userId,
      new Date(),
      newComment.value,
      newComment.content,
      id
    )
    await ProductController.uploadComment(nComment)

    const allComments = await ProductController.loadBookComment(id)
    const listOwner = allComments.map((item) => item.owner)
    const userInfo = await ProductController.loadUsersFromIds(listOwner)
    const newAllComments = allComments.map((item) => {
      return {
        ...item,
        username: userInfo[item.owner].username,
        avt: userInfo[item.owner].avt,
      }
    })
    setComments(newAllComments)
    setProduct((prev) => {
      return { ...prev, comments: newAllComments }
    })
    setNewComment(initialStateForComment)
  }
  const handlePlusAmount = () => {
    setAmount((prev) => prev + 1)
  }
  const handleMinusAmount = () => {
    setAmount((prev) => {
      return prev === 1 ? 1 : prev - 1
    })
  }

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: 'smooth' })
  }, [])

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true)
      const user = AuthenticateController.getUser()
      setUser(user)
      const allComments = await ProductController.loadBookComment(id)
      const listOwner = allComments.map((item) => item.owner)
      const userInfo = await ProductController.loadUsersFromIds(listOwner)
      const newAllComments = allComments.map((item) => {
        return {
          ...item,
          username: userInfo[item.owner].username,
          avt: userInfo[item.owner].avt,
        }
      })
      setComments(newAllComments)
      const bookDb = await ProductController.loadBook(id)
      setProduct({ ...bookDb, comments: newAllComments })
      setLoading(false)
    }
    fetchData()
  }, [id])
  if (loading) {
    return (
      <Box
        sx={{
          minHeight: '80vh',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Loading />
      </Box>
    )
  }
  return (
    <div>
      <ProductDetailContent
        product={product}
        amount={amount}
        handlePlusAmount={handlePlusAmount}
        handleMinusAmount={handleMinusAmount}
        handleAddToCart={handleAddToCart}
      />
      <Grid container flexDirection="column" sx={{ mt: 8 }} spacing={6}>
        <Grid item>
          <ProductPanel comments={comments} description={product.description} />
        </Grid>
        {user && (
          <Grid item>
            <ProductNewComment
              newComment={newComment}
              handleUpdateNewComment={handleUpdateNewComment}
              handleCreateNewComment={handleCreateNewComment}
            />
          </Grid>
        )}
      </Grid>
    </div>
  )
}

export default ProductDetailPage
