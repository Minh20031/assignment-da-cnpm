import { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { Box } from '@mui/material'
import CartMoney from '../components/CartMoney'
import CartTable from '../components/CartTable'
import OrderModal from '../components/OrderModal'
import ModalContent1 from '../components/ModalContent1'
import ModalContent2 from '../components/ModalContent2'
import ModalContent3 from '../components/ModalContent3'
import CartItem from '../components/CartItem'
import { useSelector, useDispatch } from 'react-redux'

import {
  INCREASE_IN_ORDER,
  DECREASE_IN_ORDER,
  REMOVE_FROM_CART,
  RESET_CART,
} from '../constants'
import AuthenticateController from '../controller/AuthenticateController'
import OrderController from '../controller/OrderController'
import Order from '../models/Order'

const CartPage = () => {
  const [modal, setModal] = useState(-1)
  const [province, setProvince] = useState(
    AuthenticateController.getUser()?.province || ''
  )
  const [district, setDistrict] = useState(
    AuthenticateController.getUser()?.district || ''
  )
  const [wards, setWards] = useState(
    AuthenticateController.getUser()?.wards || ''
  )
  const [address, setAddress] = useState(
    AuthenticateController.getUser()?.address || ''
  )
  const [phone, setPhone] = useState(
    AuthenticateController.getUser()?.phone || ''
  )
  const [payType, setPayType] = useState(0)
  const history = useHistory()
  const dispatch = useDispatch()
  const orderGlobal = useSelector((state) => state.cart)
  const handleIncreaseInCart = (id) => {
    dispatch({ type: INCREASE_IN_ORDER, payload: { id } })
  }
  const handleDecreaseInCart = (id) => {
    dispatch({ type: DECREASE_IN_ORDER, payload: { id } })
  }
  const handleRemoveFromCart = (id) => {
    dispatch({ type: REMOVE_FROM_CART, payload: { id } })
  }

  var user = AuthenticateController.getUser()

  if (user === null) {
    history.push('/')
    return (
      <Box
        sx={{
          height: '100vh',
        }}
      ></Box>
    )
  } else {
    return (
      <>
        <CartTable>
          {orderGlobal.map((element, index) => {
            return (
              <CartItem
                key={index}
                id={element.book.bookId}
                price={element.book.price}
                name={element.book.name}
                image={element.book.image}
                amount={element.amount}
                increase={handleIncreaseInCart}
                decrease={handleDecreaseInCart}
                deleteProduct={handleRemoveFromCart}
              />
            )
          })}
        </CartTable>
        <Box
          sx={{
            display: 'flex',
            width: '100%',
            justifyContent: { xs: 'center', sm: 'flex-end', md: 'flex-end' },
          }}
        >
          <CartMoney
            productMoney={(() => {
              var sum = 0
              orderGlobal.forEach((ele, ind) => {
                sum += ele.book.price * ele.amount
              })
              return sum
            })()}
            deliveryMoney={
              0.1 *
              (() => {
                var sum = 0
                orderGlobal.forEach((ele, ind) => {
                  sum += ele.book.price * ele.amount
                })
                return sum
              })()
            }
            onClick={() => {
              if (orderGlobal.length) setModal(0)
            }}
          ></CartMoney>
        </Box>

        {modal === -1 ? (
          <></>
        ) : (
          <OrderModal
            open={true}
            step={modal}
            buttonTitle={['Tiếp tuc', 'Hoàn tất', 'Kiểm tra']}
            buttonAction={[
              async () => {
                if (payType === 0) {
                  setModal(1)
                } else if (payType === 1) {
                  var res = await OrderController.uploadOrder(
                    Order.createOrder(
                      orderGlobal,
                      user.userId,
                      phone,
                      0.1 *
                        (() => {
                          var sum = 0
                          orderGlobal.forEach((ele) => {
                            sum += ele.book.price * ele.amount
                          })
                          return sum
                        })(),
                      false,
                      province,
                      district,
                      wards,
                      address
                    )
                  )
                  if (res) {
                    setModal(2)
                  }
                }
              },
              async () => {
                var res = await OrderController.uploadOrder(
                  Order.createOrder(
                    orderGlobal,
                    user.userId,
                    phone,
                    0.1 *
                      (() => {
                        var sum = 0
                        orderGlobal.forEach((ele) => {
                          sum += ele.book.price * ele.amount
                        })
                        return sum
                      })(),
                    true,
                    province,
                    district,
                    wards,
                    address
                  )
                )
                if (res) setModal(2)
              },
              () => {
                history.push('/user/order')
                dispatch({ type: RESET_CART })
              },
            ]}
            onBack={() => {
              if (modal === 2) setModal(-1)
              else setModal(modal - 1)
            }}
            onClose={() => {
              setProvince(AuthenticateController.getUser()?.province || '')
              setDistrict(AuthenticateController.getUser()?.district || '')
              setWards(AuthenticateController.getUser()?.wards || '')
              setAddress(AuthenticateController.getUser()?.address || '')
              setPhone(AuthenticateController.getUser()?.phone || '')
              setModal(-1)
            }}
          >
            <ModalContent1
              province={province}
              district={district}
              wards={wards}
              address={address}
              phone={phone}
              payType={payType}
              onValueChange={(
                province,
                district,
                wards,
                address,
                phone,
                payType
              ) => {
                setProvince(province)
                setDistrict(district)
                setWards(wards)
                setAddress(address)
                setPhone(phone)
                setPayType(payType)
              }}
            />
            <ModalContent2 />
            <ModalContent3 />
          </OrderModal>
        )}
      </>
    )
  }
}

export default CartPage
