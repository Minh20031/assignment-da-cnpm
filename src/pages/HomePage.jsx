import { Box, Grid, Typography } from '@mui/material'
import React, { useEffect, useState } from 'react'
import BannerProductList from '../components/BannerProductList'
import WhyUsProductList from '../components/WhyUsProductList'
import BookItem from '../components/BookItem'
import { useHistory } from 'react-router-dom'

import ProductController from '../controller/ProductController'

import Loading from '../components/LoadingComponent'

const Home = () => {
  const [products, setProducts] = useState([])
  const [loading, setLoading] = useState(true)
  const history = useHistory()

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true)
      const all_books = (await ProductController.loadBooks('Nhã Nam', 'Giáo dục')).filter(ele => ele.quantity > 0);
      setProducts(all_books)
      setLoading(false)
    }
    fetchData()
  }, [])

  if (loading) {
    return (
      <Grid container justifyContent="center" alignItems="center">
        <Grid item>
          <Loading />
        </Grid>
      </Grid>
    )
  }

  return (
    <Box
      sx={{ display: 'flex', alignItems: 'center', flexDirection: 'column' }}
    >
      <Box
        id="homepage-title"
        sx={{
          fontWeight: 700,
          fontSize: 35,
          display: 'flex',
          justifyContent: 'center',
          mb: 7,
        }}
      >
        SẢN PHẨM TIÊU BIỂU
      </Box>
      <Grid
        id="homepage-productlist"
        container
        spacing={2}
        sx={{ mb: 7, maxWidth: 1200 }}
      >
        {products.slice(0, 8).map((product) => {
          return (
            <Grid
              key={product.bookId}
              item
              xs={6}
              md={4}
              lg={3}
              sx={{
                display: {
                  // xs: key >= 4 ? 'none' : 'block',
                  // md: key >= 6 ? 'none' : 'block',
                  lg: 'block',
                },
              }}
            >
              <BookItem
                sx={{
                  width: '100%',
                  display: 'flex',
                  justifyContent: 'center',
                }}
                product={product}
              />
            </Grid>
          )
        })}
      </Grid>

      <Box
        sx={{
          fontSize: 22,
          mb: 7,
          borderBottom: 2,
          borderColor: 'secondaryColor.main',
          cursor: 'pointer',
        }}
        onClick={() => history.push(`/product`)}
      >
        <Typography color="primaryColor.main">LOAD MORE</Typography>
      </Box>

      <Box sx={{ mb: 7 }}>
        <BannerProductList />
      </Box>

      <Box sx={{ width: '100%' }}>
        <WhyUsProductList />
      </Box>
    </Box>
  )
}

export default Home
