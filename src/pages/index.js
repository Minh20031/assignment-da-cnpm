export { default as HomePage } from './HomePage'
export { default as AuthPage } from './AuthPage'

export { default as ProductListPage } from './ProductListPage'
export { default as ProductDetailPage } from './ProductDetailPage'

export { default as AdminOrderPage } from './Admin/AdminOrderPage'
export { default as AdminProductPage } from './Admin/AdminProductPage'
export { default as AdminProductDetailPage} from './Admin/AdminProductDetailPage'
export { default as AdminUpdateOrder} from './Admin/AdminUpdateOrder'

export { default as UserProfilePage } from './User/UserProfilePage'
export { default as UserOrderPage } from './User/UserOrderPage'
export { default as UserPasswordPage } from './User/UserPasswordPage'

export { default as CartPage } from './CartPage'