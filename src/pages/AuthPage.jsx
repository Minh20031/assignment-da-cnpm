import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { Grid, Typography } from "@mui/material";

import WelcomeComponent from "../components/WelcomeComponent";

import FormAuthComponent from "../components/FormAuthComponent";

import AuthClass from "../controller/AuthenticateController";

const initialForm = {
  username: "",
  email: "",
  password: "",
  confirmPassword: "",
  phone: "",
};

function validateEmail(email) {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

const checkLength = (min, max, value) => {
  return value.trim().length >= min && value.trim().length <= max;
};

const AuthPage = () => {
  const [isLogin, setIsLogin] = useState(true);
  const history = useHistory();

  const [form, setForm] = useState(initialForm);

  const handleChangeForm = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };

  const [error, setError] = useState("");

  const handleSubmitForm = async (e) => {
    e.preventDefault();

    if (isLogin) {
      const user = await AuthClass.login(form.email, form.password);
      if (!user) {
        setError("Thông tin đăng nhập không chính xác");
        return;
      }
    } else {
      if (form.confirmPassword !== form.password) {
        setError("Thông tin đăng ký không chính xác vui lòng kiểm tra lại");
        return;
      }
      if (!validateEmail(form.email) || !checkLength(1, 999, form.username)) {
        setError("Thông tin email hoặc tên đăng ký không hợp lệ");
        return;
      }
      await AuthClass.register(
        form.email,
        form.password,
        form.username,
        form.phone
      );
    }
    history.push("/");
  };
  const handleChangeState = () => setIsLogin(!isLogin);

  return (
    <Grid
      container
      alignItems="center"
      columnSpacing={3}
      sx={{ minHeight: "600px" }}
    >
      <Grid
        item
        sx={{
          width: { lg: "50%" },
          mb: 2,
          display: {
            xs: "none",
            lg: "block",
            marginBottom: "8%",
          },
        }}
      >
        <WelcomeComponent isLogin={isLogin} />
      </Grid>
      <Grid
        item
        sx={{
          minWidth: {
            lg: "50%",
            xs: "100%",
          },
        }}
      >
        <Typography color="dangerColor.main" sx={{ mb: 2 }}>
          {error}
        </Typography>
        <FormAuthComponent
          isLogin={isLogin}
          handleChangeState={handleChangeState}
          handleChangeForm={handleChangeForm}
          handleSubmitForm={handleSubmitForm}
        />
      </Grid>
    </Grid>
  );
};

export default AuthPage;
