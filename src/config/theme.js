import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    primaryColor: {
      main: '#2D9CDB',
      contrastText: '#ffffff'
    },
    secondaryColor: {
      main: '#40BFFF',
      contrastText: '#ffffff'
    },
    lightColor: {
      main: '#BCDDFE',
      contrastText: '#22262A'
    },
    grayColor: {
      main: '#e7eaec'
    },
    whiteColor: {
      main: '#ffffff',
      contrastText: '#22262A'
    },
    textColor: {
      main: '#9098B1'
    },
    titleColor: {
      main: '#22262A'
    },
    dangerColor: {
      main: '#ff4858',
      contrastText: '#ffffff'
    }
  },
  components: {
    MuiTab: {
      styleOverrides: {
        root: {
          textTransform: 'none'
        }
      }
    },
    MuiButton: {
      styleOverrides: {
        root: {
          textTransform: 'none',
          padding: '5px 8px'
        }
      }
    },
    MuiIconButton: {
      styleOverrides: {
        sizeSmall: {
          padding: '2.5px 3px',
        },
      },
    },

  },
});


export default theme;