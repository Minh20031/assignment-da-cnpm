import React, { useEffect, useState } from 'react'

import { ThemeProvider } from '@mui/material/styles'
import { Box } from '@mui/material'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import Header from './components/Header'
import Footer from './components/Footer'

import CustomerNavBar from './components/CustomerNavBar'
import AdminNavBar from './components/AdminNavbar';


import {
  HomePage,
  AuthPage,
  ProductListPage,
  ProductDetailPage,
  CartPage,
  UserProfilePage,
  UserOrderPage,
  UserPasswordPage,
  AdminProductPage,
  AdminOrderPage,
  AdminProductDetailPage,
  AdminUpdateOrder,
} from './pages'

import LoadingComponent from './components/LoadingComponent'

import './styles/global.css'
import theme from './config/theme'

import AuthenticateController from './controller/AuthenticateController'

const App = () => {

  const [init, setInit] = useState('not done')
  const [isLogin, setIsLogin] = useState(false)

  useEffect(() => {
    AuthenticateController.checkAuthState((user) => {
      if (user) {
        setIsLogin(true)
      }
      setInit('done')
    })
  }, [])
  
  
  if (init === 'not done') {
    return (
      <Box
        sx={{
          width: '100%',
          height: '100%',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          minHeight: '100vh',
        }}
      >
        <LoadingComponent />
      </Box>
    )
  }
  return (
    
    <ThemeProvider theme={theme}>
      <div className="wrapper">
        <Router>
        <Header isLogin={isLogin} setIsLogin={setIsLogin} />
          {window.location.href.includes("admin") ? <AdminNavBar/> : <CustomerNavBar />}
          <div id="container">
            <Switch>
              <Route path="/auth">
                <AuthPage />
              </Route>
              <Route path="/user/profile">
                <UserProfilePage />
              </Route>
              <Route path="/user/order">
                <UserOrderPage />
              </Route>
              <Route path="/user/password">
                <UserPasswordPage />
              </Route>
              <Route path="/product/:id">
                <ProductDetailPage />
              </Route>
              <Route path="/product">
                <ProductListPage />
              </Route>
              <Route path="/cart">
                <CartPage />
              </Route>
              <Route path="/admin/product/:id">
                <AdminProductDetailPage />
              </Route>
              <Route path="/admin/product">
                <AdminProductPage />
              </Route>
              <Route path="/admin/order/:id">
                <AdminUpdateOrder />
              </Route>
              <Route path="/admin/order">
                <AdminOrderPage />
              </Route>

              <Route exact path="/">
                <HomePage />
              </Route>
            </Switch>
          </div>
          <Footer />
        </Router>

      </div>
    </ThemeProvider>
  )
}

export default App
