export const START_LOADING = 'START_LOADING';
export const END_LOADING = 'END_LOADING';


// Cart action
export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const DECREASE_IN_ORDER = 'DECREASE_IN_ORDER'
export const INCREASE_IN_ORDER = 'INCREASE_IN_ORDER'
export const RESET_CART = 'RESET_CART'

export const colors = {
    0: '#2D9CDB',
    1: '#40BFFF',
    2: '#BCDDFE',
    3: '#FFFFFF',
    4: '#FAFAFB',
    5: '#E7EAEC',
    6: '#22262A',
    7: '#FF4858',
    8: '#223263'
}