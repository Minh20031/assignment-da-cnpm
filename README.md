## Yêu cầu

1. Phải có node. Tải bản version >= 14.x.x: Link tải: [Node JS](https://nodejs.dev/).
2. Kiểm tra version sau khi tải, mở terminal và chạy lệnh: `node -v`.
3. Cài extension của VSCode: **ES7 React/Redux/GraphQL/React-Native snippets**

## Chạy dự án:

1. `yarn install`.
2. `yarn start`. Client side được chạy trên port 3000.
3. Đợi dự án chạy cho đến khi xuất hiện thông báo: **Compiler Successfully** ở terminal.
4. Khởi động lên browser bằng cách truy cập vào đường link sau **http://localhost:3000**.
5. Không được **kill terminal** trong suốt quá trình chạy.

## Cấu trúc folder:

- public:
  - index.html: chứa mã html sẽ render ra.
  - các file còn lại là file static để config hình.
- **src**:
  - assets: Nơi chứa ảnh và các file tĩnh (nếu cần).
  - actions: Nơi chứa các hàm tương tác giữa Front-end và Back-end.
  - pages: Nơi chứa các pages cần hiện thực.
  - components: Nơi chứa các component hỗ trợ cho việc xây dựng nên 1 page.
  - config: Nơi chứa config để connect với firebase.
  - constants: Nơi chứa các biến hằng để hỗ trợ xử lý api.
  - reducers: Là một kho chứa các data từ firebase trả về.
  - styles: Nơi chứa các file **.css**.
- App.jsx: File chứa các router thay đổi chọn page.
- index.js: File entry point để connect với file **index.html** ở public.
- package.json: Chứa các package hỗ trợ cho dự án.

## Đường link truy cập page:

| Page              | Url            |
| ----------------- | -------------- |
| HomePage          | /              |
| AuthPage          | /auth          |
| ProductListPage   | /product       |
| ProductDetailPage | /product/:id   |
| CartPage          | /cart          |
| UserProfilePage   | /user/profile  |
| UserPasswordPage   | /user/password |
| UserOrderPage     | /user/order    |
| AdminOrderPage    | /admin/order   |
| AdminProductPage  | /admin/product |
| AdminUpdateOrder  | /admin/order/:id |
| AdminProductDetailPage | /admin/product/:id |
